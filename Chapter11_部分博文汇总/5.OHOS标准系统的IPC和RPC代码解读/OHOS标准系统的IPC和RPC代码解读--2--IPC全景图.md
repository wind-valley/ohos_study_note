# OHOS标准系统的IPC和RPC代码解读--2--IPC全景图

梁开祝 2022.09.25

## 1.IPC全景图

直接阅读前文《1--概述》中提到的编译目标【10】(包含【11】)的源代码，将一些关键的类、类的继承关系和调用关系逐渐整理出来，如下图所示（该图仍在继续完善中）。

![OHOH3.1_IPC](figures/OHOH3.1_IPC.jpg)

上图可以简单分成以下几个部分：

- MessageParcel 和 Parcel：这是通过IPC传输的数据对象（sendData和replyData），包含了对数据和对象的编组、扁平化(也是序列化)、打包以及逆向处理的一组功能。

- Invoker和Binder（暂未涉及DBinder）：Invoker用于与Binder驱动进行实际的交互（即通过open()、ioctl()访问Binder驱动）。

- IPCSkeleton（Thread/Process）：用于向当前进程提供获取Invoker的能力。

- 以 IRemoteObject 为起点的两个继承关系链：

  IRemoteObject<-IPCObjectStub<-IRemoteStub<-具体SA的Stub类

  IRemoteObject<-IPCObjectProxy

- (IRemoteProxy + PeerHolder)<-具体SA的Proxy类

- Broker相关的接口、类和模板定义

- 全景图下半部分的是软总线组件在IPC的实现上的粗略概括

请小伙伴们特别关注Broker部分的实现细节，它是IPC组件中一个至关重要的模块。

由于图中几个部分的关系错综复杂，我还没有整理出比较清晰的文字，所以，想深入理解OHOS的IPC实现细节的小伙伴，请结合上图阅读代码自行理解一下，有疑问可以留言交流。



## 2.SAMGR的IPC实现细节

在前文《2--samgr》中给出了samgr进程的简单启动流程，其中一个重要步骤就是向IPC组件/Binder注册samgr的远程服务对象和进程间通信上下文。

把samgr进程注册远程服务对象、samgr客户端获取samgrProxy、本地进程获取远程设备的dmsProxy的流程简单整理出来，如下图所示：

![OHOH3.1_IPC_samgr](figures/OHOH3.1_IPC_samgr.jpg)

图中流程和备注文字已经提供了足够详细的解释了，请小伙伴们结合代码自行进一步理解。



## 3.软总线组件的IPC实现细节

上面全景图的下半部分，是软总线组件服务端和客户端与实现IPC功能相关的一些粗略概括。

其他SA的IPC实现，大抵也是如此的。