# OHOS HDF 图谱--2--HdfObject对象构造器
梁开祝 2022.05.09

> 【**说明：**“OHOS HDF 图谱”系列文章，是《沉浸式剖析OpenHarmony源代码》一书第9章内容的补充材料。本系列文章将会给出大量的高清图片，要么是无法印刷到书里的超级大图（[全景图](OHOS HDF 图谱--2--HdfObject对象构造器--全景图.jpg)），要么是书籍定稿后新近整理的图片。书籍中如已经有对图片的详细解释，本系列文章将只做简单介绍；如书中没有详细解释，本系列文章将补充详细说明。】
>
> **系列文章列表：**
> [**《与OpenHarmony共同成长：一年的历程和成果展示》**](https://ost.51cto.com/posts/10799)
> [**《OHOS HDF 图谱-1-驱动配置信息树状图》**](https://ost.51cto.com/posts/12284)

OpenHarmony驱动框架，是采用C语言加面向对象编程模型来实现的（C语言如何为实现面向对象编程，请小伙伴们自行网络搜索和学习）。C++面向对象编程中，是通过构造函数来对具体类的对象进行构造和初始化的；C语言实现的驱动框架，在重要数据结构(类)实例的构造和初始化的时候，则是采用了通过objectId来查表，在对象构造器列表中调用匹配objectId的对象构造函数来生成对应的数据结构(类)实例并做初始化。本文主要用图的形式来展示驱动框架中，使用三个HdfObject对象构造器所分别生成的几个重要类对象的快照（数据结构+接口）。

## 1.对象构造器总入口
在驱动框架实现代码中，我们经常会看到类似XxxxGetInstance()或者XxxxNewInstance()这样的调用，以此获取对应类型的类对象实例。
		以获取DevmgrService类型实例的struct IDevmgrService *instance = DevmgrServiceGetInstance()为例：

```c
struct IDevmgrService *DevmgrServiceGetInstance(void)
{
    static struct IDevmgrService *instance = NULL;
    if (instance == NULL) {
        instance = (struct IDevmgrService *)  \ 
            HdfObjectManagerGetObject(HDF_OBJECT_ID_DEVMGR_SERVICE);  // 0
    }
    return instance;
}
```
这里的**HdfObjectManagerGetObject**()就是所有对象构造器的总入口，通过参数指定构造哪个类的对象。
		整个驱动框架（包括内核态和用户态），有三个对象构造器，通过HdfObjectManagerGetCreators(objectId)函数根据当前调用者所在的进程上下文环境，选用对应的对象构造器，再根据objectId参数来匹配对应的类的构造函数：

```c
struct HdfObject *HdfObjectManagerGetObject(int objectId)
{
    struct HdfObject *object = NULL;
    const struct HdfObjectCreator *targetCreator = HdfObjectManagerGetCreators(objectId);

    if ((targetCreator != NULL) && (targetCreator->Create != NULL)) {
        //1. kernel:       drivers\framework\core\common\src\devlite_object_config.c
        // get the g_liteObjectCreators[objectId].Create to creat a HdfObject
        //2. user dev_mgr: drivers\adapter\uhdf2\manager\src\devmgr_object_config.c
        // get the g_fullDevMgrObjectCreators[objectId].Create to creat a HdfObject
        //3. user dev_host:drivers\adapter\uhdf2\host\src\devhost_object_config.c
        // get the g_fullDevHostObjectCreators[objectId].Create to creat a HdfObject
        object = targetCreator->Create();
        if (object != NULL) {
            object->objectId = objectId;
        }
    }
    return object;
}
```
“**object = targetCreator->Create**()”就是调用具体类的构造函数生成具体对象了；
“**object->objectId = objectId**”这一句非常重要，object->objectId是每一个对象的身份证信息，有了它，这个具体的对象，就可以在父类、子类、孙类之间来回做强制类型转换，而不会产生类型异常等错误了，这个请小伙伴们深入理解一下。

​		对象构造器的总入口如下图所示：
![3HdfObjectCreator20220508总入口signed.jpg](figures/3-HdfObjectCreator20220508-总入口-signed.jpg)

## 2.内核态对象构造器
通过对象构造器总入口图，进入内核态部分，看一下八个类的构造函数都会生成一些什么样的对象（数据结构+接口），如下图所示：
![4HdfObjectCreator20220508内核态部分缩略图signed.jpg](figures/4-HdfObjectCreator20220508-内核态部分-signed.jpg)

## 3.用户态devmgr对象构造器
通过对象构造器总入口图，进入用户态devmgr部分，看一下三个类的构造函数都会生成一些什么样的对象（数据结构+接口），如下图所示：
![5HdfObjectCreator20220508用户态devmgr部分signed.jpg](figures/5-HdfObjectCreator20220508-用户态devmgr部分-signed.jpg)

## 4.用户态devhost对象构造器
通过对象构造器总入口图，进入用户态devhost部分，看一下八个类的构造函数都会生成一些什么样的对象（数据结构+接口），如下图所示：
![6HdfObjectCreator20220508用户态devhost部分缩略图signed.jpg](figures/6-HdfObjectCreator20220508-用户态devhost部分-signed.jpg)

## 5.小结
驱动框架在什么时候调用哪个对象构造器生成哪个对象，对象与对象之间如何相互联系、互相作用，以及各个对象的数据结构之间的网状关系如何产生、如何维护、如何使用等具体细节，在《沉浸式剖析OpenHarmony源代码》一书第9章已有非常详细的阐述了，这里暂不继续展开了。