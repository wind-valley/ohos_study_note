//lkz debug code
#define LEN  (8)   //MAX is 8, actully max level to 6
#define LENP (1)   //modify to a number <= LEN, parse and print level
const char* strsp[LEN] =
{
	"",
	"  ",
	"    ",
	"      ",
	"        ",
	"          ", //5
	"            ",
	"              ",  //7
};
static uint8_t g_nodeNum = 0;
static uint8_t g_nodeLvl = 0;

void Dbg_ParseHCStreeAttr(const char* nodeName, struct DeviceResourceAttr *attrNode, char ch, unsigned char u2_level)
{
	const char* attrName = NULL;
	uint8_t  ByteValue  = 0;
	uint16_t WordValue  = 0;
	uint32_t DwordValue = 0;
	struct DeviceResourceAttr *attr = attrNode;

	if(ch == 'a')
	{
		HDF_LOGI("%s%3d[a]:%s", strsp[u2_level], u2_level, attr?"++":"--");
		return;
	}

	#if 0
    //ch = 'A', print all attr for node: "root"
    if(strcmp(nodeName, "root") == 0)
    {
        HDF_LOGI("%s   [A]: 'module' from 'hdf.hcs', other attr from 'hdf_config_test.hcs'", strsp[u2_level]);
        while (attr)
        {
            if( (strcmp(attr->name, "match_attr") == 0) ||
                (strcmp(attr->name, "support_Device") == 0)||
                (strcmp(attr->name, "module") == 0))
            {
                attrName = attr->value + HCS_PREFIX_LENGTH;
                HDF_LOGI("%s   [A]:%s[%s]", strsp[u2_level], attr->name, attrName);
            }
            else if(strcmp(attr->name, "boardId") == 0)
            {
                WordValue = 0;
                HcsSwapToUint16(&WordValue, attr->value + HCS_PREFIX_LENGTH, HcsGetPrefix(attr->value));
                HDF_LOGI("%s   [A]:%s[%d]", strsp[u2_level], attr->name, WordValue);
            }
            else if(strcmp(attr->name, "modem_id") == 0)
            {
                uint16_t WordCount = 0;   //array Count
                HcsSwapToUint16(&WordCount, attr->value + HCS_PREFIX_LENGTH, CONFIG_WORD);

                //already know WordCount is 2, and data[0] is Dword, data[1] is Byte
                DwordValue = 0;
                HcsSwapToUint32(&DwordValue, attr->value + HCS_PREFIX_LENGTH + HCS_WORD_LENGTH + HCS_PREFIX_LENGTH,
                                HcsGetPrefix(attr->value + HCS_PREFIX_LENGTH + HCS_WORD_LENGTH));

                ByteValue = 0;
                HcsSwapToUint8(&ByteValue, attr->value + HCS_PREFIX_LENGTH + HCS_WORD_LENGTH + HCS_PREFIX_LENGTH + HCS_DWORD_LENGTH + HCS_PREFIX_LENGTH,
                                HcsGetPrefix(attr->value + HCS_PREFIX_LENGTH + HCS_WORD_LENGTH + HCS_PREFIX_LENGTH + HCS_DWORD_LENGTH));
                HDF_LOGI("%s   [A]:%s[C:%d][0x%X,0x%X]", strsp[u2_level], attr->name, WordCount, DwordValue, ByteValue);
            }
            else if(strcmp(attr->name, "board_id") == 0)
            {
                uint16_t WordCount = 0;   //array Count
                HcsSwapToUint16(&WordCount, attr->value + HCS_PREFIX_LENGTH, CONFIG_WORD);

                //already know WordCount is 2, and data[0] is Dword, data[1] is Word
                DwordValue = 0;
                HcsSwapToUint32(&DwordValue, attr->value + HCS_PREFIX_LENGTH + HCS_WORD_LENGTH + HCS_PREFIX_LENGTH,
                                HcsGetPrefix(attr->value + HCS_PREFIX_LENGTH + HCS_WORD_LENGTH));

                WordValue = 0;
                HcsSwapToUint16(&WordValue, attr->value + HCS_PREFIX_LENGTH + HCS_WORD_LENGTH + HCS_PREFIX_LENGTH + HCS_DWORD_LENGTH + HCS_PREFIX_LENGTH,
                                HcsGetPrefix(attr->value + HCS_PREFIX_LENGTH + HCS_WORD_LENGTH + HCS_PREFIX_LENGTH + HCS_DWORD_LENGTH));
                HDF_LOGI("%s   [A]:%s[C:%d][%d,%d]", strsp[u2_level], attr->name, WordCount, DwordValue, WordValue);
            }
            attr = attr->next;
        }
    }
    else
    #endif
    #if 1
    //ch = 'A', print all attr for node gpio_config: "controller_0x120d0000"
    if(strcmp(nodeName, "controller_0x120d0000") == 0)
    {
		/* controller_0x120d0000 {
                match_attr = "hisilicon_hi35xx_pl061";
                groupNum = 12;
                bitNum = 8;
                regBase = 0x120d0000;
                regStep = 0x1000;
                irqStart = 48;
                irqShare = 0;
        }*/
        while (attr)
        {
            if(strcmp(attr->name, "match_attr") == 0)
            {
                attrName = attr->value + HCS_PREFIX_LENGTH;
                HDF_LOGI("%s   [A]:%s[%s]", strsp[u2_level], attr->name, attrName);
            }
            else if(strcmp(attr->name, "groupNum") == 0)
            {
                ByteValue = 0;
                HcsSwapToUint8(&ByteValue, attr->value + HCS_PREFIX_LENGTH, HcsGetPrefix(attr->value));
                HDF_LOGI("%s   [A]:%s[%d]", strsp[u2_level], attr->name, ByteValue);
            }
            else if(strcmp(attr->name, "bitNum") == 0)
            {
                ByteValue = 0;
                HcsSwapToUint8(&ByteValue, attr->value + HCS_PREFIX_LENGTH, HcsGetPrefix(attr->value));
                HDF_LOGI("%s   [A]:%s[%d]", strsp[u2_level], attr->name, ByteValue);
            }
            else if(strcmp(attr->name, "regBase") == 0)
            {
                DwordValue = 0;
                HcsSwapToUint32(&DwordValue, attr->value + HCS_PREFIX_LENGTH, HcsGetPrefix(attr->value));
                HDF_LOGI("%s   [A]:%s[0x%X]", strsp[u2_level], attr->name, DwordValue);
            }
            else if(strcmp(attr->name, "regStep") == 0)
            {
                WordValue = 0;
                HcsSwapToUint16(&WordValue, attr->value + HCS_PREFIX_LENGTH, HcsGetPrefix(attr->value));
                HDF_LOGI("%s   [A]:%s[0x%X]", strsp[u2_level], attr->name, WordValue);
            }
            else if(strcmp(attr->name, "irqStart") == 0)
            {
                ByteValue = 0;
                HcsSwapToUint8(&ByteValue, attr->value + HCS_PREFIX_LENGTH, HcsGetPrefix(attr->value));
                HDF_LOGI("%s   [A]:%s[%d]", strsp[u2_level], attr->name, ByteValue);
            }
            else if(strcmp(attr->name, "irqShare") == 0)
            {
                ByteValue = 0;
                HcsSwapToUint8(&ByteValue, attr->value + HCS_PREFIX_LENGTH, HcsGetPrefix(attr->value));
                HDF_LOGI("%s   [A]:%s[%d]", strsp[u2_level], attr->name, ByteValue);
            }

            attr = attr->next;
        }
    }
    else
    #endif
    {
        HDF_LOGI("%s%3d[A]:%s", strsp[u2_level], u2_level, attr?"++":"--");
    }
}

void Dbg_ParseHCStreeSub(struct DeviceResourceNode *node, char ch, unsigned char u2_level)
{
	if((node == NULL)||(u2_level >= LENP)||(u2_level >= LEN))
	{
		return;
	}

	g_nodeNum ++;  //node number++
	g_nodeLvl = (g_nodeLvl>=u2_level)?(g_nodeLvl):(u2_level);   //max node level

	//[n]
	HDF_LOGI("%s%3d[n]:NodeName[%s]", strsp[u2_level], u2_level,node->name);
	//[a]
	char c = 'a';
	if( //(strcmp(node->name, "root") == 0) ||
		(strcmp(node->name, "controller_0x120d0000") == 0) ||
		//(strcmp(node->name, "led_config") == 0) ||
	    0)
	{
		c = 'A';
	}
	Dbg_ParseHCStreeAttr(node->name, node->attrData, c, u2_level);

	//[p]
	HDF_LOGI("%s%3d[p]:%s", strsp[u2_level], u2_level, node->parent?(node->parent->name):"--");

	//[c]
	HDF_LOGI("%s%3d[c]:%s", strsp[u2_level], u2_level, node->child?(node->child->name):"--");
	Dbg_ParseHCStreeSub(node->child, 'c', u2_level+1);



	#if 1
	//[s]
	HDF_LOGI("%s%3d[s]:%s", strsp[u2_level], u2_level, node->sibling?(node->sibling->name):"--");
	if(node->sibling != NULL)
	{
		if(node->parent == node->sibling->parent)
		{
			HDF_LOGI("%s------", strsp[u2_level]);
			Dbg_ParseHCStreeSub(node->sibling, 's', u2_level);
		}
		else
		{
			HDF_LOGI("%s======", strsp[u2_level]);
			Dbg_ParseHCStreeSub(node->sibling, 's', u2_level+1);
		}
	}
	#endif
}

void Dbg_ParseHCStree(void)
{
	if(g_hcsTreeRoot == NULL)
		return;

	#if 0
	struct DeviceResourceNode {
		const char *name;					   /**< Pointer to the node name */
		uint32_t hashValue; 				   /**< Unique ID of a node */
		struct DeviceResourceAttr *attrData;   /**< Pointer to the node attributes */
		struct DeviceResourceNode *parent;	   /**< Pointer to the parent node */
		struct DeviceResourceNode *child;	   /**< Pointer to a child node */
		struct DeviceResourceNode *sibling;    /**< Pointer to a sibling node */
	};
	#endif

	HDF_LOGI("===========================Dbg_ParseHCStree:===========================");
	HDF_LOGI("[r]-root,[n]-node,[a]-attr,[p]-parent,[c]-child,[s]-sibling,[++]-items,[--]-NULL");
	HDF_LOGI("[r]:%s",g_hcsTreeRoot->name);

	#if 1
	Dbg_ParseHCStreeSub(g_hcsTreeRoot,'r', 0);
	HDF_LOGI("[r]:Node Num[%d], Max Level[%d]",g_nodeNum, g_nodeLvl);
	#else
	HDF_LOGI("   ......");
	#endif

	HDF_LOGI("===========================Dbg_ParseHCStree.===========================");
}
