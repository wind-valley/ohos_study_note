
## Booting kernel from Legacy Image at 82000000 ...
   Image Name:   Linux-5.10.57
   Image Type:   ARM Linux Kernel Image (uncompressed)
   Data Size:    4922412 Bytes = 4.7 MiB
   Load Address: 80008000
   Entry Point:  80008000
   Loading Kernel Image

Starting kernel ...

[lkz_kernel][//kernel/linux/linux-5.10/]
[lkz_kernel][init/main.c] start_kernel: Begin:
[lkz_kernel][arch/arm/kernel/setup.c] smp_setup_processor_id:
[lkz_kernel] Booting Linux on physical CPU 0x0
[lkz_kernel][linux_banner]:
Linux version 5.10.57 (ohos@ubuntu) (OHOS (release) clang version 10.0.1.73276  (llvm-project 434bcd0f84635e92ea9da9bf7748a6cf24a8e627), GNU ld (GNU Binutils for Ubuntu) 2.34) #1 SMP Sat Jan 1 20:44:06 CST 2022

[lkz_kernel][init/main.c] start_kernel: setup_arch()[@arch/arm/kernel/setup.c]
CPU: ARMv7 Processor [410fc075] revision 5 (ARMv7), cr=10c5387d
CPU: div instructions available: patching division code
CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
OF: fdt: Machine model: Hisilicon HI3516DV300 DEMO Board
Memory policy: Data cache writealloc
cma: Reserved 16 MiB at 0x87000000
Zone ranges:
  Normal   [mem 0x0000000080000000-0x0000000087ffffff]
  HighMem  empty
Movable zone start for each node
Early memory node ranges
  node   0: [mem 0x0000000080000000-0x0000000087ffffff]
Initmem setup node 0 [mem 0x0000000080000000-0x0000000087ffffff]
percpu: Embedded 14 pages/cpu s27532 r8192 d21620 u57344
Built 1 zonelists, mobility grouping on.  Total pages: 32512

[lkz_kernel][init/main.c] Kernel command line: mem=128M console=ttyAMA0,115200 root=/dev/mmcblk0p3 rw rootfstype=ext4 rootwait selinux=0 rootdelay=5 blkdevparts=mmcblk0:1M(boot),9M(kernel),50M(rootfs),50M(userfs)

Dentry cache hash table entries: 16384 (order: 4, 65536 bytes, linear)
Inode-cache hash table entries: 8192 (order: 3, 32768 bytes, linear)
mem auto-init: stack:off, heap alloc:off, heap free:off
Memory: 101068K/131072K available (8192K kernel code, 610K rwdata, 1796K rodata, 1024K init, 366K bss, 13620K reserved, 16384K cma-reserved, 0K highmem)
SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=2, Nodes=1
rcu: Hierarchical RCU implementation.
rcu: RCU calculated value of scheduler-enlistment delay is 10 jiffies.
NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
Gic dist init...
random: get_random_bytes called from start_kernel+0x1ec/0x358 with crng_init=0
arch_timer: CPU0: Trapping CNTVCT access
arch_timer: cp15 timer(s) running at 50.00MHz (phys).
clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0xb8812736b, max_idle_ns: 440795202655 ns
sched_clock: 56 bits at 50MHz, resolution 20ns, wraps every 4398046511100ns
Switching to timer-based delay loop, resolution 20ns
Console: colour dummy device 80x30
Calibrating delay loop (skipped), value calculated using timer frequency.. 100.00 BogoMIPS (lpj=500000)
pid_max: default: 32768 minimum: 301
Mount-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
CPU: Testing write buffer coherency: ok
[lkz_kernel][init/main.c] rest_init: kernel_thread(kernel_init)

[lkz_kernel][init/main.c] kernel_init: Begin:
[lkz_kernel][init/main.c] kernel_init: kernel_init_freeable()
CPU0: thread -1, cpu 0, socket 0, mpidr 80000000
[lkz_kernel][init/main.c] kernel_init_freeable: do_pre_smp_initcalls()
Setting up static identity map for 0x80100000 - 0x80100070
rcu: Hierarchical SRCU implementation.
smp: Bringing up secondary CPUs ...
arch_timer: CPU1: Trapping CNTVCT access
CPU1: thread -1, cpu 1, socket 0, mpidr 80000001
smp: Brought up 1 node, 2 CPUs
SMP: Total of 2 processors activated (200.00 BogoMIPS).
CPU: All CPU(s) started in SVC mode.
[lkz_kernel][init/main.c] kernel_init_freeable: do_basic_setup()
[lkz_kernel][init/main.c] do_basic_setup: driver_init()[@drivers/base/init.c]
devtmpfs: initialized
[lkz_kernel][init/main.c] do_basic_setup: do_initcalls()
VFP support v0.3: implementor 41 architecture 2 part 30 variant 7 rev 5
clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
futex hash table entries: 512 (order: 3, 32768 bytes, linear)
pinctrl core: initialized pinctrl subsystem
NET: Registered protocol family 16
DMA: preallocated 256 KiB pool for atomic coherent allocations
Serial: AMBA PL011 UART driver
120a0000.uart: ttyAMA0 at MMIO 0x120a0000 (irq = 29, base_baud = 0) is a PL011 rev2
printk: console [ttyAMA0] enabled
uart-pl011 120a1000.uart: aliased and non-aliased serial devices found in device tree. Serial port enumeration may be unpredictable.
120a1000.uart: ttyAMA1 at MMIO 0x120a1000 (irq = 30, base_baud = 0) is a PL011 rev2
SCSI subsystem initialized
usbcore: registered new interface driver usbfs
usbcore: registered new interface driver hub
usbcore: registered new device driver usb
mc: Linux media interface: v0.10
videodev: Linux video capture interface: v2.00
Advanced Linux Sound Architecture Driver Initialized.
clocksource: Switched to clocksource arch_sys_counter
NET: Registered protocol family 2
IP idents hash table entries: 2048 (order: 2, 16384 bytes, linear)
tcp_listen_portaddr_hash hash table entries: 512 (order: 0, 6144 bytes, linear)
TCP established hash table entries: 1024 (order: 0, 4096 bytes, linear)
TCP bind hash table entries: 1024 (order: 1, 8192 bytes, linear)
TCP: Hash tables configured (established 1024 bind 1024)
UDP hash table entries: 256 (order: 1, 8192 bytes, linear)
UDP-Lite hash table entries: 256 (order: 1, 8192 bytes, linear)
NET: Registered protocol family 1
RPC: Registered named UNIX socket transport module.
RPC: Registered udp transport module.
RPC: Registered tcp transport module.
RPC: Registered tcp NFSv4.1 backchannel transport module.
workingset: timestamp_bits=30 max_order=15 bucket_order=0
NFS: Registering the id_resolver key type
Key type id_resolver registered
Key type id_legacy registered
NET: Registered protocol family 38
Block layer SCSI generic (bsg) driver version 0.4 loaded (major 250)
io scheduler mq-deadline registered
io scheduler kyber registered
pl061_gpio 120d0000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d1000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d2000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d3000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d4000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d5000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d6000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d7000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d8000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120d9000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120da000.gpio_chip: PL061 GPIO chip registered
pl061_gpio 120db000.gpio_chip: PL061 GPIO chip registered
brd: module loaded
loop: module loaded
hisi_nand: Failed to locate of_node [id: 0]
libphy: Fixed MDIO Bus: probed
libphy: hisi_femac_mii_bus: probed
hisi-femac 10010000.ethernet: using random MAC address 62:c3:04:27:b5:c0
Generic PHY 10011100.mdio:01: attached PHY driver [Generic PHY] (mii_bus:phy_addr=10011100.mdio:01, irq=POLL)
phy_id=0x001cc816, phy_mode=rmii
usbcore: registered new interface driver r8152
xhci-hcd 100e0000.xhci_0: xHCI Host Controller
xhci-hcd 100e0000.xhci_0: new USB bus registered, assigned bus number 1
xhci-hcd 100e0000.xhci_0: hcc params 0x0220fe6c hci version 0x110 quirks 0x0000000020010010
xhci-hcd 100e0000.xhci_0: irq 39, io mem 0x100e0000
hub 1-0:1.0: USB hub found
hub 1-0:1.0: 1 port detected
xhci-hcd 100e0000.xhci_0: xHCI Host Controller
xhci-hcd 100e0000.xhci_0: new USB bus registered, assigned bus number 2
xhci-hcd 100e0000.xhci_0: Host supports USB 3.0 SuperSpeed
usb usb2: We don't know the algorithms for LPM for this host, disabling LPM.
hub 2-0:1.0: USB hub found
hub 2-0:1.0: hub can't support USB3.0
usbcore: registered new interface driver usb-storage
mousedev: PS/2 mouse device common for all mice
usbcore: registered new interface driver xpad
hibvt_rtc 12080000.rtc: registered as rtc0
hibvt_rtc 12080000.rtc: setting system clock to 1970-01-01T00:00:51 UTC (51)
hibvt_rtc 12080000.rtc: RTC driver for hibvt enabled
i2c /dev entries driver
hibvt-i2c 120b0000.i2c: IRQ index 0 not found
hibvt-i2c 120b0000.i2c: hibvt-i2c0@100000hz registered
hibvt-i2c 120b1000.i2c: IRQ index 0 not found
hibvt-i2c 120b1000.i2c: hibvt-i2c1@100000hz registered
hibvt-i2c 120b2000.i2c: IRQ index 0 not found
hibvt-i2c 120b2000.i2c: hibvt-i2c2@100000hz registered
hibvt-i2c 120b3000.i2c: IRQ index 0 not found
hibvt-i2c 120b3000.i2c: hibvt-i2c3@100000hz registered
hibvt-i2c 120b4000.i2c: IRQ index 0 not found
hibvt-i2c 120b4000.i2c: hibvt-i2c4@100000hz registered
hibvt-i2c 120b5000.i2c: IRQ index 0 not found
hibvt-i2c 120b5000.i2c: hibvt-i2c5@100000hz registered
hibvt-i2c 120b6000.i2c: IRQ index 0 not found
hibvt-i2c 120b6000.i2c: hibvt-i2c6@100000hz registered
hibvt-i2c 120b7000.i2c: IRQ index 0 not found
hibvt-i2c 120b7000.i2c: hibvt-i2c7@100000hz registered
usbcore: registered new interface driver uvcvideo
USB Video Class driver (1.1.1)
sp805-wdt 12051000.watchdog: registration successful
device-mapper: ioctl: 4.43.0-ioctl (2020-10-01) initialised: dm-devel@redhat.com
himci: mmc host probe
himci: sdio_setup entry!
himci: mmc host probe
himci: mmc host probe
mmc0: new high speed MMC card at address 0001
mmcblk0: mmc0:0001 8GTF4R 7.28 GiB
mmcblk0boot0: mmc0:0001 8GTF4R partition 1 4.00 MiB
mmcblk0boot1: mmc0:0001 8GTF4R partition 2 4.00 MiB
mmcblk0rpmb: mmc0:0001 8GTF4R partition 3 512 KiB, chardev (248:0)
 mmcblk0: p1(boot) p2(kernel) p3(rootfs) p4(userfs)
usbcore: registered new interface driver usbhid
usbhid: USB HID core driver
         hilog_init Start0
NET: Registered protocol family 10
Segment Routing with IPv6
NET: Registered protocol family 17
8021q: 802.1Q VLAN Support v1.8
Key type dns_resolver registered
Registering SWP/SWPB emulation handler

##########################################################
[devmgr_load] late_initcall(DeviceManagerInit): DeviceManagerStart() Begin:
[I][02505/hdf_wifi_core] HdfWifiDriverBind(g_hdfWifiEntry): StartMessageRouter() and dev->service = &wifiService
[I][02505/K_message_router] DoStartMessageRouter: Create local node ...
[I][02505/K_local_node] Creating local node...
[I][02505/K_message_router] Register default dispatcher...
[I][02505/hdf_wifi_core] HdfWifiDriverBind(g_hdfWifiEntry): OK

[I][02505/hdf_wifi_core] HdfWlanMainInit: g_hdfWifiEntry, Begin:
[I][02505/hdf_wifi_core] HdfWlanMainInit: [4-1]HdfWlanGetConfig()
[I][02505/hdf_wlan_config_parser] ParseWlanChipsCompsConfig: driverName=hisi
[I][02505/hdf_wifi_core] HdfWlanMainInit: [4-2]HdfWlanGetModuleConfigRoot() && parse
[I][02505/hdf_wifi_core] HdfWlanMainInit: [4-3]HdfWlanInitProduct()
[I][02505/hdf_wifi_product] HdfWlanInitProduct: InitWifiModule()
[I][02505/wifi_module] InitWifiModule: InitFeatures()
[I][02505/wifi_module] InitFeatures: BaseInit()
[I][02505/wifi_base] BaseInit(): g_baseService = CreateServiceBaseService()
[W][02505/K_message_router] DoRegistService:Register service Node:0;Dispatcher:0;Service:1
[I][02505/K_sidecar] InitService: return Service *service...OK

[I][02505/ap] ApInit(): g_apService = CreateServiceAPService()
[W][02505/K_message_router] DoRegistService:Register service Node:0;Dispatcher:0;Service:2
[I][02505/K_sidecar] InitService: return Service *service...OK

[I][02505/sta] StaInit(): g_staService = CreateServiceSTAService()
[W][02505/K_message_router] DoRegistService:Register service Node:0;Dispatcher:0;Service:3
[I][02505/K_sidecar] InitService: return Service *service...OK

[I][02505/wifi_module] InitWifiModule: InitFeatures() hdf wifi module init succ
[I][02505/hdf_wifi_core] HdfWlanMainInit: [4-4]HdfWlanScanAndInitThread()
[I][02505/hdf_wifi_core] HdfWlanMainInit: g_hdfWifiEntry, End

[I][02505/hdf_wifi_core] HdfWlanInitThread: OsalSleep(15s) wait for 'hi3881_fw.bin' mount and load
[devmgr_load] late_initcall(DeviceManagerInit): DeviceManagerStart() End. : OK!
##########################################################

ALSA device list:
  No soundcards found.
[lkz_kernel][init/main.c] kernel_init_freeable: prepare_namespace()[@init/do_mounts.c]
[lkz_kernel][init/do_mounts.c] prepare_namespace: Waiting 5 sec before mounting root device...
mmc2: new high speed SDIO card at address 0001
mmc1: new high speed SDHC card at address 59b4
mmcblk1: mmc1:59b4 SMI   3.75 GiB
 mmcblk1: p1
EXT4-fs (mmcblk0p3): mounted filesystem with ordered data mode. Opts: (null)
VFS: Mounted root (ext4 filesystem) on device 179:3.
devtmpfs: mounted
Freeing unused kernel memory: 1024K
[lkz_kernel][init/main.c] kernel_init: system_state[SYSTEM_RUNNING]
[lkz_kernel][init/main.c] kernel_init: execute_command[NULL], ... End.
[lkz_kernel][init/main.c] run_init_process: Run '/sbin/init' as init process:
[lkz_kernel][init/main.c] run_init_process: Run '/etc/init' as init process:
[lkz_kernel][init/main.c] run_init_process: Run '/bin/init' as init process:



##########################################################
##########################################################
[init_lite] "/bin/init" main: starting OHOS 'Lite'[Linux] Framework...
[I][00719/Init] [init_lite] main[8-2] PrintSysInfo(common):
[I][00719/Init] *Sl*/*Sl*/*Sl*/*Sl*/OpenHarmony-3.0.0.0(LTS)/*Sl*/*Sl*/7/OpenHarmony 2.3 beta/debug
[I][00719/Init] [init_lite] main[8-4] SignalInitModule()
[I][00719/Init] [init_lite] main[8-5] ExecuteRcs(): run 'sh /etc/init.d/rcS'
[I][00719/Init] ExecuteRcs, child process id 67: /etc/init.d/rcS

.........����ȥһ��log��

[I][00719/Init] SigHandler, SIGCHLD received, sigPID = 67.
[I][00719/Init] [init_lite] main[8-6] InitReadCfg() Begin:
[I][00719/Init] InitReadCfg[4-2]: ParseInitCfg: /etc/init.cfg && Import
[I][00719/Init] ServiceStart: Name[softbus_server]: OK!!
[I][00719/Init] ServiceStart: Name[devicemanagerservice]: OK!!
[I][00719/Init] InitReadCfg[4-4.3]: DoJob(post-init)
[I][00719/Init] InitReadCfg[4-4]: ReleaseAllJobs()
[I][00719/Init] [init_lite] main[8-6] InitReadCfg() End.
[I][00719/Init] [init_lite] main, time used: sigInfo 0 ms, rcs 777 ms, cfg 17 ms.
[I][00719/Init] [init_lite] main[8-8] while(1)...
[init_lite]###################End Of###################[init_lite]
##################################################################

.........����ȥһ��log��

[I][02505/hdf_wifi_core] -------------------------------------------------------
[I][02505/hdf_wifi_core] HdfWlanInitThread: OsalSleep(15s) timeout: Begin HDF_WIFI initing
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-1]HdfWlanGetDeviceList() from hcs/hcb
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-2]HdfWlanConfigSDIO(busId[2])
[I][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Config Hi3516DV300 SDIO bus 2
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x12090000] 101e0 to 101e0
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f0008] 06d1 to 0601
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f000c] 05f1 to 0501
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f0010] 05f1 to 0501
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f0014] 05f1 to 0501
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f0018] 05f1 to 0501
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f001c] 05f1 to 0501
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-3]ProbeDevice(): PowerOn/Reset/Init
[I][02505/hdf_wifi_core] ProbeDevice: [3-1]HdfWlanPowerOnProcess(device->powers)
[I][02505/hdf_wifi_core] HdfWlanPowerOnProcess: Chip PowerOn!
[I][02505/hdf_wifi_core] ProbeDevice: [3-2]HdfWlanResetProcess(device->reset)
[I][02505/hdf_wifi_core] HdfWlanResetProcess:   Chip Reset!
[I][02505/hdf_wifi_core] ProbeDevice: [3-3]HdfWlanBusInit(deviceConfig->bus)
[I][02505/hdf_wifi_core] HdfWlanBusInit: driver name[hisi]
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-4]HdfWlanAddDevice(hisi)
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-5]DevSvcManagerClntSubscribeService()
[I][02505/hdf_driver_register__Hi3881] HdfWlanHisiChipDriverInit(g_hdfHisiChipEntry): HDFWlanRegHisiDriverFactory()
[I][02505/hdf_driver_register__Hi3881] HDFWlanRegHisiDriverFactory: driverMgr = HdfWlanGetChipDriverMgr(Wlan)
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanGetChipDriverMgr(): return g_chipDriverManager
[I][02505/hdf_driver_register__Hi3881] HDFWlanRegHisiDriverFactory: driverMgr->RegChipDriver(&tmpFactory): hisi
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanRegChipDriver(hisi)
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanRegChipDriver(hisi): HdfChipDriverFactory[0] registered OK.
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-6]HdfWifiInitDevice()
[I][02505/hdf_wifi_core] HdfWifiInitDevice: chipDriverFact = HdfWlanGetDriverFactory(device->driverName)
[I][02505/hdf_wifi_core] HdfWlanGetDriverFactory: initMgr  = HdfWlanGetChipDriverMgr()
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanGetChipDriverMgr(): return g_chipDriverManager
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanGetChipDriverByName(hisi): return HdfChipDriverFactory[0]
[I][02505/hdf_wifi_core] HdfWifiInitDevice: chipDriverFact->InitChip(device)
[I][02505/hdfinit_3881__Hi3881] InitHi3881Chip: hi_wifi_init()

================================================
[hi_wifi_driver_api][linux] hi_wifi_init: Begin:
  oal_register_ioctl:
  hi_wifi_plat_init:
  [wal_main] hi_wifi_plat_init: Begin:
    hi_wifi_plat_init:
      [oam_main] oam_main_init SUCCESSFULLY!
    wal_customize_init:
      [wal_customize] wal_customize_init SUCCESSFULLY!
    oal_main_init:
      [oal_main] oal_main_init SUCCESSFULLY
    frw_main_init:
      [frw_main] frw_main_init SUCCESSFULLY!
  [wal_main] hi_wifi_plat_init: End. SUCCESSFULLY
  hi_wifi_host_init:
    [wal_main] hi_wifi_host_init: Begin:
      hcc_hmac_init:
mmc2: card 0001 removed
mmc2: new high speed SDIO card at address 0001
        [hcc_hmac] hcc_hmac_init SUCCESSFULLY
      plat_firmware_init:
          [plat_firmware] firmware_parse_cfg: Hi3881_VERSION:: [Hi3881V100]
        [plat_firmware] plat_firmware_init SUCCESSFULLY
      wlan_pm_open:
              [plat_pm_wlan] wlan_pm_open: Begin: set_device_is_ready(F)
                wlan_power_on:
[plat_pm] wlan_power_on:: firmware_download_function:
[plat_pm] firmware_download_function:: firmware_download:
[plat_firmware] firmware_download:: Begin:
[plat_firmware] firmware_download:: firmware_mem_request()
[plat_firmware] firmware_download:: execute_download_cmd(VERSION:Hi3881V100)
[plat_firmware] firmware_download:: execute_download_cmd(FILES:1,0xe4800,/vendor/firmware/hi3881/hi3881_fw.bin)
[plat_firmware] firmware_download:: execute_download_cmd(CONFIG:0xe4800,4)
[plat_firmware] firmware_download:: execute_download_cmd(:)
[plat_firmware] firmware_download:: End.
[plat_pm] wlan_power_on:: firmware_download_function: success
                set_device_is_ready(T)!
                wlan_pm_init:
              [plat_pm_wlan] wlan_pm_open: End. SUCCESSFULLY!
      hmac_main_init:
[hmac_vap] hmac_vap_creat_netdev(ifName[Hisilicon0]) -> NetDeviceInit()
[I][02505/net_device] NetDeviceInit(Hisilicon0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl Success!
[I][02505/net_device_adapter] NetDevInit Success!
[I][02505/net_device] NetDeviceInit(Hisilicon0) success!
[I][02505/net_device_adapter] NetDevAdd success!!
        [hmac_main] hmac_main_init SUCCESSULLY
      firmware_sync_cfg_paras_to_wal_customize: wifi_cfg->wal_customize
      wal_main_init:
        [wal_main] wal_main_init Begin:
        [wal_main] wal_main_init SUCCESSFULLY
    [wal_main] hi_wifi_host_init: End. SUCCESSFULLY
[hi_wifi_driver_api][linux] hi_wifi_init: End. successfully
================================================
[I][02505/hdf_wifi_core] HdfWifiInitDevice: HdfWlanInitInterfaces(device, chipDriverFact)
[I][02505/hdf_driver_register__Hi3881] BuildHi3881Driver()->HiMac80211Init()+Hi3881Init()::
[I][02505/net_device] NetDeviceInit(wlan0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl Success!
[I][02505/net_device_adapter] NetDevInit Success!
[I][02505/net_device] NetDeviceInit(wlan0) success!
[I][02505/hdfinit_3881__Hi3881] Hi3881Init: WAL_WIFI_MODE_STA
[I][02505/net_device] NetDeviceInit(p2p0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl Success!
[I][02505/net_device_adapter] NetDevInit Success!
[I][02505/net_device] NetDeviceInit(p2p0) success!
[I][02505/eapol] CreateEapolData success!
[I][02505/net_device_adapter] NetDevAdd success!!
[I][02505/hdfinit_3881__Hi3881] Hi3881Init: wal_init_drv_wlan_netdev()
[I][02505/eapol] CreateEapolData success!
[I][02505/net_device_adapter] NetDevAdd success!!
[I][02505/hdfinit_3881__Hi3881] Hi3881Init: End.

[I][02505/hdf_wifi_core] HdfWlanInitInterfaces:hisi init interfaces finished!
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-7]End. HDF_WIFI inited SUCCESSULLY
[I][02505/hdf_wifi_core] -------------------------------------------------------


[wifilink] WiFiLink: Begin:
[wifilink] WiFiLink[3-1] dlopen(libwpa)  OK.
[wifilink] WiFiLink[3-2] dlsym(wpa_main) OK.
[wifilink] WiFiLink[3-3] func: wpa_main(...)
================================================
//third_party/wpa_supplicant/wpa_supplicant-2.9/
[wpa_supplicant/main.c] wpa_main: Begin:

[wpa_supplicant/wpa_supplicant] wpa_supplicant_init: ver:2.9
[wpa_supplicant/eloop] eloop_init:
[wpa_supplicant/eloop] eloop_ctrl_init: 0
[wpa_supplicant/main] Successfully initialized wpa_supplicant
[wpa_supplicant/wpa_hal] WifiWpaInit: ifName[wlan0]
[wpa_supplicant/wpa_hal] WifiClientInit: WifiDriverClientInit().
[I][02505/wifi_base] WifiCmdSetMode: 'wlan0' changing mode to 2 ...
[I][02515/(null)] [sbuf_cmd_adapter] WifiDriverClientInit
[wpa_supplicant/wpa_hal] WifiClientInit: WifiRegisterEventCallback(func=0xb6e1d4a8, type[11519], ifName[wlan0])
[I][02515/(null)] [wifi_driver_client]   WifiRegisterEventCallback(func=0xb6e1d4a8, type[11519], ifName[wlan0])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdSetNetdev: SendCmdSync(WIFI_WPA_CMD_SET_NETDEV)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65547])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdSetMode: SendCmdSync(WIFI_WPA_CMD_SET_MODE)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65545])
[I][02505/net_device_adapter] NetDevDelete success!
[E][02505/eapol] DestroyEapolData success!
[I][02505/net_device_adapter] net device linux deinit success!
[I][02505/net_device_adapter] UnRegisterNetDeviceImpl success.
[I][02505/net_device] DeInitNetDeviceImpl success!
[I][02505/net_device] NetDeviceInit(wlan0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl Success!
[I][02505/net_device_adapter] NetDevInit Success!
[I][02505/net_device] NetDeviceInit(wlan0) success!
[I][02505/eapol] CreateEapolData success!
[I][02505/net_device_adapter] NetDevAdd success!!
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdSetNetdev: SendCmdSync[0](WIFI_WPA_CMD_SET_NETDEV)
[W:1416][hmac_config] {hmac_config_start_vap:host start vap ok}
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65547])
[I][02505/net_device] NetIfSetStatus: ndImpl->interFace->setStatus(ndImpl)
[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolEnable: SendCmdSync(WIFI_WPA_CMD_ENALBE_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65542])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdGetOwnMac: SendCmdSync(WIFI_WPA_CMD_GET_ADDR)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65544])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdGetHwFeature: SendCmdSync(WIFI_WPA_CMD_GET_HW_FEATURE)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65546])
[wpa_supplicant/wpa_hal] WifiWpaGetHwFeatureData done
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02505/sta] ScanAll: chipDriver->staOps->StartScan(netDev, params)
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
WIFI: Scan : (null) SSID : 0

[wpa_supplicant/wpa_hal] WifiWpaScan2: WifiCmdScan(ifName[wlan0])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdScan: SendCmdSync(WIFI_WPA_CMD_SCAN)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[196610])

ifconfig wlan0 192.168.1.11
Ping 192.168.1.1 (192.168.1.1): 56(84) bytes.
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: receive event=4
[I][02515/(null)] [wifi_driver_client] WifiEventReport(ifName[wlan0], event[4][WIFI_EVENT_SCAN_DONE]) -->> onRecFunc[0xb6e1d4a8]
[I][02505/sta] WifiCmdAssoc:wlan0 connecting to AP ...
[I][02505/sta] Connect: chipDriver->staOps->Connect(netDev, param)

[wpa_supplicant/wpa_hal] WifiWpaGetScanResults2 done
[wpa_supplicant/wpa_hal_event] WifiWpaEventScanDoneProcess done
wlan0: [wpa_supplicant/wpa_supplicant] Trying to associate with 4a:8c:21:20:67:65 (SSID='TP-LINK_LKZ' freq=2437 MHz)

[wpa_supplicant/wpa_hal] WifiWpaTryConnect: WifiCmdAssoc(ifName[wlan0])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdAssoc: SendCmdSync(WIFI_WPA_CMD_ASSOC)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[196608])
[wpa_supplicant/wpa_hal] WifiWpaAssociate done ret=0

[E][02505/hdf_wifi_event] HdfWifiEventConnectResult::start dhcp client failed
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: **ignore many** event=5[WIFI_EVENT_SCAN_RESULT]
wlan0: Associated with 4a:8c:21:20:67:65
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: receive event=6
[I][02515/(null)] [wifi_driver_client] WifiEventReport(ifName[wlan0], event[6][WIFI_EVENT_CONNECT_RESULT]) -->> onRecFunc[0xb6e1d4a8]
[I][02505/net_device] NetIfRxImpl specialEtherType Process not need TCP/IP stack!

wlan0: CTRL-EVENT-[I][02505/wifi_base] WifiCmdReceiveEapol:receiveEapol ret=0
SUBNET-STATUS-UPDATE status=0
[wpa_supplicant/wpa_hal_event] WifiWpaEventConnectResultProcess done
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: **ignore many** event=5[WIFI_EVENT_SCAN_RESULT]
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: receive event=13
[I][02505/net_device] NetIfRxImpl specialEtherType Process not need TCP/IP stack!

[I][02515/(null)] [wifi_driver_client] WifiEventReport(ifName[wlan0], event[13][WIFI_EVENT_EAPOL_RECV]) -->> onRecFunc[0xb6e1d4a8]
[I][02505/wifi_base] WifiCmdReceiveEapol:receiveEapol ret=0

[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolPacketReceive: SendCmdSync(WIFI_WPA_CMD_RECEIVE_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65541])
[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolPacketSend: SendCmdSync(WIFI_WPA_CMD_SEND_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65540])
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: **ignore many** event=5[WIFI_EVENT_SCAN_RESULT]
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: receive event=13
[I][02515/(null)] [wifi_driver_client] WifiEventReport(ifName[wlan0], event[13][WIFI_EVENT_EAPOL_RECV]) -->> onRecFunc[0xb6e1d4a8]

[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolPacketReceive: SendCmdSync(WIFI_WPA_CMD_RECEIVE_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65541])
[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolPacketSend: SendCmdSync(WIFI_WPA_CMD_SEND_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65540])
wlan0: wpa: Key negotiation completed with: 4a:8c:21:20:67:65 [PTK=CCMP GTK=CCMP]


wlan0: CTRL-EVENT-CONNECTED - Connection to 4a:8c:21:20:67:65 completed [id=0 id_str=]
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdNewKey: SendCmdSync(WIFI_WPA_CMD_NEW_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65536])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdSetKey: SendCmdSync(WIFI_WPA_CMD_SET_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65538])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdNewKey: SendCmdSync(WIFI_WPA_CMD_NEW_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65536])
64 bytes from 192.168.1.1: icmp_seq=1 ttl=0 time=2060 ms
64 bytes from 192.168.1.1: icmp_seq=2 ttl=0 time=1052 ms
64 bytes from 192.168.1.1: icmp_seq=3 ttl=0 time=52 ms

--- 192.168.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss
round-trip min/avg/max = 0/0/1054 ms
ping 192.168.1.1


#ifconfig
lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope: Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:0 TX bytes:0

wlan0     Link encap:Ethernet  HWaddr 54:f1:5f:28:23:51
          inet addr:192.168.1.11  Bcast:192.168.1.255  Mask:255.255.255.0
          inet6 addr: fe80::56f1:5fff:fe28:2351/64 Scope: Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:1 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:0 TX bytes:0

#
