/* Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: liangkz @ https://ost.51cto.com/column/46
 * Date  : 2021.12.24
 *
 */
//#include <ohos_errno.h>
#include <dlfcn.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define NUM  (3)
static int   g_wpaArgc = NUM;
static char* g_wpaArgv[NUM] = {"", "-iwlan0", "-c/etc/wifilink.conf"};

static void* WiFiLinkLinux(void)
{
    sleep(1);
    //printf("[wifilink] WiFiLinkLinux: Begin:\n");

    #define BUF_SIZE (80)
    char buf[BUF_SIZE] = {0};
    char FixedIP[BUF_SIZE] = {0};
    char PingWho[BUF_SIZE] = {0};
    int  len = 0;

    FILE* fd = fopen("/etc/wifilink.conf", "r");
    if(fd != NULL)
    {
        while (fgets(buf, BUF_SIZE-1, fd))
        {
            if(strncmp(buf, "#FixedIP=", 9) == 0)
            {
                //#FixedIP=192.168.1.11
                sprintf(FixedIP,"ifconfig wlan0 %s", buf+9);
                len = strlen(FixedIP);
                if(len > 0)
                    FixedIP[len-1] = 0;
                //printf("[wifilink] FixedIP: [%d]%s\n",len,FixedIP);
            }
            if(strncmp(buf, "#PingWho=", 9) == 0)
            {
                //#PingWho=192.168.1.107
                sprintf(PingWho,"ping %s", buf+9);
                len = strlen(PingWho);
                if(len > 0)
                    PingWho[len-1] = 0;
                //printf("[wifilink] PingWho: [%d]%s\n",len,PingWho);
            }
        }
        fclose(fd);
    }

    if(FixedIP[0] == 0)
        sprintf(FixedIP,"ifconfig wlan0 192.168.1.20");

    system(FixedIP);
    //printf("[wifilink] FixedIP: system(%s)\n",FixedIP);
    printf("%s\n",FixedIP);

    system("echo 0 9999999 > /proc/sys/net/ipv4/ping_group_range");
    //printf("[wifilink] Permission: system(echo 0 9999999 > /proc/sys/net/ipv4/ping_group_range)\n");

    //if(PingWho[0] == 0)
    //    sprintf(PingWho,"ping www.baidu.com");
    //system(PingWho);
    //printf("[wifilink] PingWho: system(%s)\n",PingWho);
    //printf("%s\n",PingWho);

    //printf("[wifilink] WiFiLinkLinux End.\n");

    return (void*)0;
}

static int DelaySec = 0;
static void* WiFiLink(void)
{
    //see:
    //drivers/framework/model/network/wifi/core/hdf_wifi_core.c
    //static int32_t HdfWlanInitThread(void *para)
    //const uint32_t initDelaySec = 15;
    //OsalSleep(initDelaySec);
    //wait 15s for 'hi3881_fw.bin' mount and load, reset and init hi3881
    //so here wait ''DelaySec' for wlan0 hi3881 inited
    printf("[wifilink] WiFiLink: sleep(%d)\n",DelaySec);
    sleep(DelaySec);
    
    //printf("[20211224] <<OHOS Study Note>>liangkz@https://ost.51cto.com/column/46\n");

    printf("[wifilink] WiFiLink: Begin:\n");
    //step 1: load libwpa.so
    void *handleLibWpa = dlopen("/usr/lib/libwpa.so", RTLD_NOW | RTLD_LOCAL);
    printf("[wifilink] WiFiLink[3-1] dlopen(libwpa)  %s.\n",(handleLibWpa == NULL)?"NG":"OK");
    if (handleLibWpa == NULL) {
        printf("[wifilink] dlopen failed!\n");
        return (void*)-1;
    }

    //step 2: get "wpa_main" API handle into "func"
    int (*func)(int, char **) = NULL;
    func =  dlsym(handleLibWpa, "wpa_main");
    printf("[wifilink] WiFiLink[3-2] dlsym(wpa_main) %s.\n",(func == NULL)?"NG":"OK");
    if (func == NULL) {
        printf("[wifilink] dlsym failed!\n");
        dlclose(handleLibWpa);
        return (void*)-1;
    }

    //step 3: call func(wpa_main) with argc/argv
    /*
    int   g_wpaArgc = NUM;
    char* g_wpaArgv[NUM] = {0};
    g_wpaArgv[0] = "-Dxxx";                 //-D driver's name[xxx]
    g_wpaArgv[1] = "-iwlan0";               //-i interface's name
    g_wpaArgv[2] = "-c/etc/wifilink.conf";  //-c config file's name
    g_wpaArgv[3] = "-B";                    //run in background ??
    */

#if (defined __LINUX__)
    pthread_t g_LinuxThread;
    if (0 != pthread_create(&g_LinuxThread, NULL, WiFiLinkLinux, NULL)) {
        printf("[wifilink] create WiFiLinkLinux failed\n");
        return 1;
    }
    //pthread_join(g_LinuxThread, NULL);
    pthread_detach(g_LinuxThread);
#endif

    printf("[wifilink] WiFiLink[3-3] func: wpa_main(...)\n");
    int ret = func(g_wpaArgc, g_wpaArgv);
    printf("[wifilink] WiFiLink[3-3] func: wpa_main(...) ret[%d] %s.\n",ret, (ret == 0)?"OK":"NG");
    if (ret != 0) {
        printf("[wifilink] wpa_main failed!\n");
        dlclose(handleLibWpa);
        return (void*)-1;
    }

    if (dlclose(handleLibWpa) != 0) {
        printf("[wifilink] dlclose failed!\n");
        return (void*)-1;
    }

    printf("[wifilink] WiFiLink[3-0] End.\n");

    return (void*)0;
}

int main(int argc, char **argv)
{
    if (argc >= 2) {
        DelaySec = atoi(argv[1]);
    }
    printf("[wifilink] main Begin: DelaySec[%d]\n",DelaySec);

    printf("[wifilink] main create WiFiLink\n");
    pthread_t g_LinkThread;
    if (0 != pthread_create(&g_LinkThread, NULL, WiFiLink, NULL)) {
        printf("[wifilink] create WiFiLink failed\n");
        return 1;
    }

    pthread_join(g_LinkThread, NULL);
    //pthread_detach(g_LinkThread);

    printf("[wifilink] main End.\n");

    return 0;
}
