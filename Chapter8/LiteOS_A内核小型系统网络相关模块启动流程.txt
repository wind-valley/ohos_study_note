
## Starting application at 0x80000000 ...

[los_config] OsMain: Begin:
[los_config] OsMain: EarliestInit()
[sdk_liteos/board] LOS_MODULE_INIT(HisiSmpInit, LOS_INIT_LEVEL_EARLIEST)
[los_config] OsMain: ArchEarlyInit()
[los_config] OsMain: PlatformEarlyInit()
[los_config] OsMain: OsSystemInfo()

******************Welcome******************

Processor   : Cortex-A7 * 2
Run Mode    : SMP
GIC Rev     : GICv2
build time  : Dec 31 2021 13:30:18
Kernel      : Huawei LiteOS 2.0.0.37/debug

*******************************************

        main core booting up...
[los_config] OsMain: OsTaskInit()
[los_config] OsMain: OsSysMemInit()
[los_config] OsMain: OsIpcInit()
[los_config] OsMain: OsSystemProcessCreate()
[los_config] OsMain: ArchInit()
[los_config] OsMain: PlatformInit()

[autoconf] LOS_MODULE_INIT(OsBsdInit, LOS_INIT_LEVEL_PLATFORM):
[autoconf] OsBsdInit()->configure()->nexus_init(nexus.c)->UsbResourceInit()&&machine_resource_init()
[los_config] OsMain: KModInit()
[los_config] OsMain: OsSmpInit()
[sdk_liteos/board] release_secondary_cores: cpuNum[1], startEntry[0x80000020]
[los_config] OsMain: End.

OsSchedStart: cpu 1 entering scheduler
OsSchedStart: cpu 0 entering scheduler

=========================================
[system_init] SystemInit: Begin:
[system_init] SystemInit_HDFInit: DeviceManagerStart(HDF) Begin:

[I][02505/hdf_driver_register] HdfEthHisiChipDriverInit(g_hdfHisiEthChipEntry): HdfEthRegHisiDriverFactory()
[I][02505/hdf_driver_register] HdfEthRegHisiDriverFactory: Begin:
[I][02505/hdf_driver_register] HdfEthRegHisiDriverFactory: driverMgr = HdfEthGetChipDriverMgr(ETH)
[I][02505/hdf_driver_register] HdfEthRegHisiDriverFactory: driverMgr->RegChipDriver(&tmpFactory): hieth-sf
[I][02505/eth_chip_driver] HdfEthRegChipDriver:Chip driver hieth-sf registed.
[I][02505/hdf_driver_register] HdfEthRegHisiDriverFactory: End. OK
[I][02505/hdf_eth_core] HdfEthDriverInit: hdf eth driver framework init Begin:
[D][02505/eth_device] ParseEthDevListNode: deviceListSize=1
[I][02505/net_device] NetDeviceInit(eth0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl: register lite netdevice impl success.
[I][02505/net_device_adapter] LiteNetDevInit: Success!
[I][02505/net_device] NetDeviceInit(eth0) success!
[I][02505/hieth_phy] InitHiethDriver: Begin:
[I][02505/net_device_adapter] netifapi_netif_add success!
[I][02505/net_device_adapter] LiteNetDevAdd success!!
[I][02505/interface] HiethInit: Begin:
[I][02505/interface] HiethHwInit: Begin:
[E][02505/interface] hisi_eth: User did not set phy mode, use default=rmii
[E][02505/interface] hisi_eth: User did not set phy addr, auto scan...
[E][02505/ctrl] festa PHY wait autotrim done timeout!
[I][02505/interface] HiethHwInit: Detected phy addr 1, phyid: 0x1cc816.
[E][02505/eth_phy] waiting for auto-negotiation completed!
[I][02505/net_device] NetIfSetLinkStatus: ndImpl->interFace->setLinkStatus(ndImpl)
[I][02505/interface] HiethLinkStatusChanged: Link is Down

[I][02505/net_device] NetIfSetAddr: ndImpl->interFace->setIpAddr(ndImpl)
[I][02505/net_device] NetIfSetStatus: ndImpl->interFace->setStatus(ndImpl)
[I][02505/hdf_eth_core] HdfEthDriverInit: hdf eth driver framework init End. success
[I][02505/hdf_wifi_core] HdfWifiDriverBind(g_hdfWifiEntry): StartMessageRouter() and dev->service = &wifiService
[I][02505/K_message_router] DoStartMessageRouter: Create local node ...
[I][02505/K_local_node] Creating local node...
[I][02505/K_message_router] Register default dispatcher...
[I][02505/hdf_wifi_core] HdfWifiDriverBind(g_hdfWifiEntry): OK

[I][02505/hdf_wifi_core] HdfWlanMainInit: g_hdfWifiEntry, Begin:
[I][02505/hdf_wifi_core] HdfWlanMainInit: [4-1]HdfWlanGetConfig()
[D][02505/hdf_wlan_config_parser] ParseWlanStaConfig: name=sta, mode=1!
[D][02505/hdf_wlan_config_parser] ParseWlanApConfig: name=ap, mode=0, vapResNum=1, userResNum=8!
[D][02505/hdf_wlan_config_parser] ParseWlanP2PConfig: name=p2p, mode=0
[D][02505/hdf_wlan_config_parser] ParseWlanMac80211Config: mode=1
[D][02505/hdf_wlan_config_parser] ParseWlanPhyConfig: mode=1

[D][02505/hdf_wlan_config_parser] ParseWlanModuleConfig: featureMap=ffff, msgName=WalService
[D][02505/hdf_wlan_config_parser] ParseWlanDevListConfig: deviceListSize=1
[I][02505/hdf_wlan_config_parser] ParseWlanChipsCompsConfig: driverName=hisi
[D][02505/hdf_wlan_config_parser] ParseWlanChipsConfig: chipSize=1
[I][02505/hdf_wifi_core] HdfWlanMainInit: [4-2]HdfWlanGetModuleConfigRoot() && parse
[I][02505/hdf_wifi_core] HdfWlanMainInit: [4-3]HdfWlanInitProduct()
[I][02505/hdf_wifi_product] HdfWlanInitProduct: InitWifiModule()
[I][02505/wifi_module] InitWifiModule: InitFeatures()
[I][02505/wifi_module] InitFeatures: BaseInit()

[I][02505/wifi_base] BaseInit(): g_baseService = CreateServiceBaseService()
[W][02505/K_message_router] DoRegistService:Register service Node:0;Dispatcher:0;Service:1
[I][02505/K_sidecar] InitService: return Service *service...OK

[I][02505/ap] ApInit(): g_apService = CreateServiceAPService()
[W][02505/K_message_router] DoRegistService:Register service Node:0;Dispatcher:0;Service:2
[I][02505/K_sidecar] InitService: return Service *service...OK

[I][02505/sta] StaInit(): g_staService = CreateServiceSTAService()
[W][02505/K_message_router] DoRegistService:Register service Node:0;Dispatcher:0;Service:3
[I][02505/K_sidecar] InitService: return Service *service...OK

[I][02505/wifi_module] InitWifiModule: InitFeatures() hdf wifi module init succ
[I][02505/hdf_wifi_core] HdfWlanMainInit: [4-4]HdfWlanScanAndInitThread()
[I][02505/hdf_wifi_core] HdfWlanMainInit: g_hdfWifiEntry, End

[I][02505/hdf_wifi_core] HdfWlanInitThread: OsalSleep(15s) wait for 'hi3881_fw.bin' mount and load

[system_init] SystemInit_HDFInit: DeviceManagerStart(HDF) End.
[system_init] SystemInit: End.
=========================================



##########################################################
##########################################################
[init_lite] "/bin/init" main: starting OHOS 'Lite'[LiteOS_A] Framework...
[I][00719/Init] [init_lite] main[8-2] PrintSysInfo(common):
[I][00719/Init] *Sa*/*Sa*/*Sa*/*Sa*/OpenHarmony-3.0.0.0(LTS)/*Sa*/*Sa*/7/OpenHarmony 2.3 beta/debug
[I][00719/Init] [init_lite] main[8-4] SignalInitModule()
[I][00719/Init] [init_lite] main[8-6] InitReadCfg() Begin:
[I][00719/Init] InitReadCfg[4-2]: ParseInitCfg: /etc/init.cfg && Import: /etc/init.*.cfg
[I][00719/Init] InitReadCfg[4-3]: ParseOtherCfgs(/system/etc/init/*.cfg)
[E][00719/Init] ParseCfgs open cfg dir :/system/etc/init failed.2
[I][00719/Init] InitReadCfg[4-3]: ParseOtherCfgs done.

[I][00719/Init] InitReadCfg[4-4.1]: DoJob(pre-init)
[I][00719/Init] ServiceStart: Name[foundation]: OK!!
[I][00719/Init] ServiceStart: Name[bundle_daemon]: OK!!
[I][00719/Init] ServiceStart: Name[appspawn]: OK!!
[I][00719/Init] ServiceStart: Name[media_server]: OK!!
[I][00719/Init] ServiceStart: Name[wms_server]: OK!!
[I][00719/Init] ServiceStart: Name[shell]: OK!!

[I][00719/Init] InitReadCfg[4-4.2]: DoJob(init)
[I][00719/Init] ServiceStart: Name[apphilogcat]: OK!!
[I][00719/Init] ServiceStart: Name[deviceauth_service]: OK!!
[I][00719/Init] ServiceStart: Name[sensor_service]: OK!!
[I][00719/Init] ServiceStart: Name[ai_server]: OK!!
[I][00719/Init] ServiceStart: Name[wifilink]: OK!!
[I][00719/Init] ServiceStart: Name[softbus_server]: OK!!

[I][00719/Init] InitReadCfg[4-4.3]: DoJob(post-init)
[I][00719/Init] InitReadCfg[4-4]: ReleaseAllJobs()
[I][00719/Init] [init_lite] main[8-6] InitReadCfg() End.
[I][00719/Init] [init_lite] main, time used: sigInfo 5 ms, rcs 0 ms, cfg 2693 ms.
[I][00719/Init] [init_lite] main[8-8] while(1)...
[init_lite]###################End Of###################[init_lite]
##################################################################

.........����ȥһ��log��

[I][02505/hdf_wifi_core] -------------------------------------------------------
[I][02505/hdf_wifi_core] HdfWlanInitThread: OsalSleep(15s) timeout: Begin HDF_WIFI initing
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-1]HdfWlanGetDeviceList() from hcs/hcb
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-2]HdfWlanConfigSDIO(busId[2])
[I][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Config Hi3516DV300 SDIO bus 2
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x12090000] 101e0 to 101e0
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f0008] 06b1 to 0601
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f000c] 05e1 to 0501
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f0010] 05e1 to 0501
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f0014] 05e1 to 0501
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f0018] 05e1 to 0501
[W][02505/hdf_wlan_sdio_adapt] ConfigHi3516DV300SDIO: Change register[0x112f001c] 05e1 to 0501
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-3]ProbeDevice(): PowerOn/Reset/Init
[I][02505/hdf_wifi_core] ProbeDevice: [3-1]HdfWlanPowerOnProcess(device->powers)
[I][02505/hdf_wifi_core] HdfWlanPowerOnProcess: Chip PowerOn!
[I][02505/hdf_wifi_core] ProbeDevice: [3-2]HdfWlanResetProcess(device->reset)
[I][02505/hdf_wifi_core] HdfWlanResetProcess:   Chip Reset!
[I][02505/hdf_wifi_core] ProbeDevice: [3-3]HdfWlanBusInit(deviceConfig->bus)
[I][02505/hdf_wifi_core] HdfWlanBusInit: driver name[hisi]

[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-4]HdfWlanAddDevice(hisi)
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-5]DevSvcManagerClntSubscribeService()
[I][02505/hdf_driver_register__Hi3881] HdfWlanHisiChipDriverInit(g_hdfHisiChipEntry): HDFWlanRegHisiDriverFactory()
[I][02505/hdf_driver_register__Hi3881] HDFWlanRegHisiDriverFactory: driverMgr = HdfWlanGetChipDriverMgr(Wlan)
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanGetChipDriverMgr(): return g_chipDriverManager
[I][02505/hdf_driver_register__Hi3881] HDFWlanRegHisiDriverFactory: driverMgr->RegChipDriver(&tmpFactory): hisi
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanRegChipDriver(hisi)
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanRegChipDriver(hisi): HdfChipDriverFactory[0] registered OK.
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-6]HdfWifiInitDevice()
[I][02505/hdf_wifi_core] HdfWifiInitDevice: chipDriverFact = HdfWlanGetDriverFactory(device->driverName)
[I][02505/hdf_wifi_core] HdfWlanGetDriverFactory: initMgr  = HdfWlanGetChipDriverMgr()
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanGetChipDriverMgr(): return g_chipDriverManager
[I][02505/hdf_wlan_chipdriver_manager] HdfWlanGetChipDriverByName(hisi): return HdfChipDriverFactory[0]
[I][02505/hdf_wifi_core] HdfWifiInitDevice: chipDriverFact->InitChip(device)
[I][02505/hdfinit_3881__Hi3881] InitHi3881Chip: hi_wifi_init()

----------------------------------------
[hi_wifi_driver_api][liteos] hi_wifi_init: Begin:
  [wal_main] hi_wifi_plat_init: Begin:
    hi_wifi_plat_init:
      [oam_main] oam_main_init SUCCESSFULLY!
    wal_customize_init:
      [wal_customize] wal_customize_init SUCCESSFULLY!
    oal_main_init:
      [oal_main] oal_main_init SUCCESSFULLY
    frw_main_init:
      [frw_main] frw_main_init SUCCESSFULLY!
  [wal_main] hi_wifi_plat_init: End. SUCCESSFULLY
    [wal_main] hi_wifi_host_init: Begin:
      hcc_hmac_init:
        [hcc_hmac] hcc_hmac_init SUCCESSFULLY
      plat_firmware_init:
          [plat_firmware] firmware_parse_cfg: Hi3881_VERSION:: [Hi3881V100]
        [plat_firmware] plat_firmware_init SUCCESSFULLY
      wlan_pm_open:
              [plat_pm_wlan] wlan_pm_open: Begin: set_device_is_ready(F)
                wlan_power_on:
[plat_pm] wlan_power_on:: firmware_download_function:
[plat_pm] firmware_download_function:: firmware_download:
[plat_firmware] firmware_download:: Begin:
[plat_firmware] firmware_download:: firmware_mem_request()
[plat_firmware] firmware_download:: execute_download_cmd(VERSION:Hi3881V100)
[plat_firmware] firmware_download:: execute_download_cmd(FILES:1,0xe4800,/vendor/firmware/hi3881/hi3881_fw.bin)
[plat_firmware] firmware_download:: execute_download_cmd(CONFIG:0xe4800,4)
[plat_firmware] firmware_download:: execute_download_cmd(:)
[plat_firmware] firmware_download:: End.
[plat_pm] wlan_power_on:: firmware_download_function: success
                set_device_is_ready(T)!
                wlan_pm_init:
              [plat_pm_wlan] wlan_pm_open: End. SUCCESSFULLY!
      hmac_main_init:
[hmac_vap] hmac_vap_creat_netdev(ifName[Hisilicon0]) -> NetDeviceInit()
[I][02505/net_device] NetDeviceInit(Hisilicon0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl: register lite netdevice impl success.
[I][02505/net_device_adapter] LiteNetDevInit: Success!
[I][02505/net_device] NetDeviceInit(Hisilicon0) success!
        [hmac_main] hmac_main_init SUCCESSULLY
      wal_main_init:
        [wal_main] wal_main_init Begin:
        [wal_main] wal_main_init SUCCESSFULLY
    [wal_main] hi_wifi_host_init: End. SUCCESSFULLY
[hi_wifi_driver_api][liteos] hi_wifi_init: End. successfully
----------------------------------------

[I][02505/hdf_wifi_core] HdfWifiInitDevice: HdfWlanInitInterfaces(device, chipDriverFact)
[I][02505/hdf_driver_register__Hi3881] BuildHi3881Driver()->HiMac80211Init()+Hi3881Init()::
[I][02505/net_device] NetDeviceInit(wlan0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl: register lite netdevice impl success.
[I][02505/net_device_adapter] LiteNetDevInit: Success!
[I][02505/net_device] NetDeviceInit(wlan0) success!
[I][02505/hdfinit_3881__Hi3881] Hi3881Init: WAL_WIFI_MODE_STA
[I][02505/net_device] NetDeviceInit(p2p0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl: register lite netdevice impl success.
[I][02505/net_device_adapter] LiteNetDevInit: Success!
[I][02505/net_device] NetDeviceInit(p2p0) success!
[I][02505/eapol] CreateEapolData success!
[I][02505/net_device_adapter] netifapi_netif_add success!
[I][02505/net_device_adapter] LiteNetDevAdd success!!
[I][02505/hdfinit_3881__Hi3881] Hi3881Init: wal_init_drv_wlan_netdev()
[I][02505/eapol] CreateEapolData success!
[I][02505/net_device_adapter] netifapi_netif_add success!
[I][02505/net_device_adapter] LiteNetDevAdd success!!
[I][02505/hdfinit_3881__Hi3881] Hi3881Init: End.

[I][02505/hdf_wifi_core] HdfWlanInitInterfaces:hisi init interfaces finished!
[D][02505/hdf_wifi_core] HdfWifiInitDevice:init interfaces success! netIfMap=1.
[I][02505/hdf_wifi_core] HdfWlanInitThread: [7-7]End. HDF_WIFI inited SUCCESSULLY
[I][02505/hdf_wifi_core] -------------------------------------------------------


[wifilink] WiFiLink: Begin:
[wifilink] WiFiLink[3-1] dlopen(libwpa)  OK.
[wifilink] WiFiLink[3-2] dlsym(wpa_main) OK.
[wifilink] WiFiLink[3-3] func: wpa_main(...)
================================================
//third_party/wpa_supplicant/wpa_supplicant-2.9/
[wpa_supplicant/main.c] wpa_main: Begin:

[wpa_supplicant/wpa_supplicant] wpa_supplicant_init: ver:2.9
[wpa_supplicant/eloop] eloop_init:
[wpa_supplicant/eloop] eloop_ctrl_init: 0
[wpa_supplicant/main] Successfully initialized wpa_supplicant
[wpa_supplicant/wpa_hal] WifiWpaInit: ifName[wlan0]

[wpa_supplicant/wpa_hal] WifiClientInit: WifiDriverClientInit().
[I][02515/(null)] [sbuf_cmd_adapter] WifiDriverClientInit

[wpa_supplicant/wpa_hal] WifiClientInit: WifiRegisterEventCallback(func=0x2273937c, type[11519], ifName[wlan0])
[I][02515/(null)] [wifi_driver_client]   WifiRegisterEventCallback(func=0x2273937c, type[11519], ifName[wlan0])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdSetNetdev: SendCmdSync(WIFI_WPA_CMD_SET_NETDEV)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65547])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdSetMode: SendCmdSync(WIFI_WPA_CMD_SET_MODE)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65545])
[I][02505/wifi_base] WifiCmdSetMode: 'wlan0' changing mode to 2 ...
[I][02505/net_device_adapter] LiteNetDevDelete success!
[E][02505/eapol] DestroyEapolData success!
[I][02505/net_device_adapter] LiteNetDevDeInit: net device lite deinit success!
[I][02505/net_device_adapter] UnRegisterNetDeviceImpl success.
[I][02505/net_device] DeInitNetDeviceImpl success!
[I][02505/net_device] NetDeviceInit(wlan0):
[I][02505/net_device_adapter] RegisterNetDeviceImpl: register lite netdevice impl success.
[I][02505/net_device_adapter] LiteNetDevInit: Success!
[I][02505/net_device] NetDeviceInit(wlan0) success!
[I][02505/eapol] CreateEapolData success!
[I][02505/net_device_adapter] netifapi_netif_add success!
[I][02505/net_device_adapter] LiteNetDevAdd success!!
[29639][W:1416][hmac_config] {hmac_config_start_vap:host start vap ok}
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdSetNetdev: SendCmdSync(WIFI_WPA_CMD_SET_NETDEV)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65547])
[I][02505/net_device] NetIfSetAddr: ndImpl->interFace->setIpAddr(ndImpl)
[I][02505/net_device] NetIfSetStatus: ndImpl->interFace->setStatus(ndImpl)
[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolEnable: SendCmdSync(WIFI_WPA_CMD_ENALBE_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65542])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdGetOwnMac: SendCmdSync(WIFI_WPA_CMD_GET_ADDR)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65544])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdGetHwFeature: SendCmdSync(WIFI_WPA_CMD_GET_HW_FEATURE)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65546])
[D][02505/wifi_base] WifiFillHwFeature: supported rate 10
[D][02505/wifi_base] WifiFillHwFeature: supported rate 20
[D][02505/wifi_base] WifiFillHwFeature: supported rate 55
[D][02505/wifi_base] WifiFillHwFeature: supported rate 110
[D][02505/wifi_base] WifiFillHwFeature: supported rate 60
[D][02505/wifi_base] WifiFillHwFeature: supported rate 90
[D][02505/wifi_base] WifiFillHwFeature: supported rate 120
[D][02505/wifi_base] WifiFillHwFeature: supported rate 180
[D][02505/wifi_base] WifiFillHwFeature: supported rate 240
[D][02505/wifi_base] WifiFillHwFeature: supported rate 360
[D][02505/wifi_base] WifiFillHwFeature: supported rate 480
[D][02505/wifi_base] WifiFillHwFeature: supported rate 540
[wpa_supplicant/wpa_hal] WifiWpaGetHwFeatureData done
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdDelKey: SendCmdSync(WIFI_WPA_CMD_DEL_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65537])
WIFI: Scan : (null) SSID : 0

[wpa_supplicant/wpa_hal] WifiWpaScan2: WifiCmdScan(ifName[wlan0])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdScan: SendCmdSync(WIFI_WPA_CMD_SCAN)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[196610])
[I][02505/sta] ScanAll: chipDriver->staOps->StartScan(netDev, params)
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: receive event=4
[I][02515/(null)] [wifi_driver_client] WifiEventReport(ifName[wlan0], event[4][WIFI_EVENT_SCAN_DONE]) -->> onRecFunc[0x2273937c]
[wpa_supplicant/wpa_hal] WifiWpaGetScanResults2 done
[wpa_supplicant/wpa_hal_event] WifiWpaEventScanDoneProcess done

wlan0: [wpa_supplicant/wpa_supplicant] Trying to associate with 4a:8c:21:20:67:65 (SSID='TP-LINK_LKZ' freq=2437 MHz)
[wpa_supplicant/wpa_hal] WifiWpaTryConnect: WifiCmdAssoc(ifName[wlan0])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdAssoc: SendCmdSync(WIFI_WPA_CMD_ASSOC)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[196608])
[I][02505/sta] WifiCmdAssoc:wlan0 connecting to AP ...
[I][02505/sta] Connect: chipDriver->staOps->Connect(netDev, param)
[wpa_supplicant/wpa_hal] WifiWpaAssociate done ret=0
[I][02505/net_device] NetIfDhcpStop: ndImpl->interFace->dhcpStop(ndImpl)
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: **ignore many** event=5[WIFI_EVENT_SCAN_RESULT]
[I][02505/net_device_adapter] LiteNetDhcpStop success!
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: receive event=6
[I][02505/net_device] NetIfDhcpStart: ndImpl->interFace->dhcpStart(ndImpl)
[I][02515/(null)] [wifi_driver_client] WifiEventReport(ifName[wlan0], event[6][WIFI_EVENT_CONNECT_RESULT]) -->> onRecFunc[0x2273937c]

[I][02505/net_device_adapter] LiteNetDhcpStart success!
[31706][W:670][net_adapter] {wal_report_sta_assoc_info::report sta conn succ to lwip!}
[I][02505/net_device] NetIfRxImpl specialEtherType Process not need TCP/IP stack!
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: **ignore many** event=5[WIFI_EVENT_SCAN_RESULT]
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: receive event=13
[I][02515/(null)] [wifi_driver_client] WifiEventReport(ifName[wlan0], event[13][WIFI_EVENT_EAPOL_RECV]) -->> onRecFunc[0x2273937c]

wlan0: Associated with 4a:8c:21:20:67:65
wlan0: CTRL-EVENT-SUBNET-STATUS-UPDATE status=0
[wpa_supplicant/wpa_hal_event] WifiWpaEventConnectResultProcess done
[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolPacketReceive: SendCmdSync(WIFI_WPA_CMD_RECEIVE_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65541])
[I][02505/wifi_base] WifiCmdReceiveEapol:receiveEapol ret=0
[I][02505/net_device] NetIfRxImpl specialEtherType Process not need TCP/IP stack!
[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolPacketSend: SendCmdSync(WIFI_WPA_CMD_SEND_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65540])
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: **ignore many** event=5[WIFI_EVENT_SCAN_RESULT]
[I][02515/(null)] [sbuf_event_adapter] OnWiFiEvents: receive event=13
[I][02515/(null)] [wifi_driver_client] WifiEventReport(ifName[wlan0], event[13][WIFI_EVENT_EAPOL_RECV]) -->> onRecFunc[0x2273937c]

[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolPacketReceive: SendCmdSync(WIFI_WPA_CMD_RECEIVE_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65541])
[I][02505/wifi_base] WifiCmdReceiveEapol:receiveEapol ret=0
[I][02515/(null)] [sbuf_cmd_adapter] WifiEapolPacketSend: SendCmdSync(WIFI_WPA_CMD_SEND_EAPOL)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65540])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdNewKey: SendCmdSync(WIFI_WPA_CMD_NEW_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65536])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdSetKey: SendCmdSync(WIFI_WPA_CMD_SET_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65538])
[I][02515/(null)] [sbuf_cmd_adapter] WifiCmdNewKey: SendCmdSync(WIFI_WPA_CMD_NEW_KEY)
[I][02515/(null)] [sbuf_cmd_adapter] SendCmdSync: g_wifiService->dispatcher->Dispatch(cmd[65536])
wlan0: wpa: Key negotiation completed with: 4a:8c:21:20:67:65 [PTK=CCMP GTK=CCMP]


wlan0: CTRL-EVENT-CONNECTED - Connection to 4a:8c:21:20:67:65 completed [id=0 id_str=]
[I][02505/net_device] NetIfDhcpIsBound: ndImpl->interFace->dhcpIsBound(ndImpl)
[I][02505/net_device] NetIfDhcpIsBound: ndImpl->interFace->dhcpIsBound(ndImpl)
[I][02505/net_device] NetIfDhcpIsBound: ndImpl->interFace->dhcpIsBound(ndImpl)
[I][02505/net_device] NetIfDhcpIsBound: ndImpl->interFace->dhcpIsBound(ndImpl)
[I][02505/net_device] NetIfDhcpIsBound: ndImpl->interFace->dhcpIsBound(ndImpl)
[I][02505/net_device_adapter] LiteNetDhcpIsBound success!


OHOS # ifconfig
wlan1   ip:192.168.1.107 netmask:255.255.255.0 gateway:192.168.1.1
        HWaddr 54:f1:5f:28:23:51 MTU:1500 Running Default Link UP
wlan0   ip:0.0.0.0 netmask:0.0.0.0 gateway:0.0.0.0
        HWaddr 54:f1:5f:28:23:54 MTU:1500 Stop Link UP
lo      ip:127.0.0.1 netmask:255.0.0.0 gateway:127.0.0.1
        ip6: ::1/64
        HWaddr 00 MTU:0 Running Link UP
eth0    ip:192.168.1.10 netmask:255.255.255.0 gateway:192.168.1.1
        HWaddr f2:99:ba:e1:c9:bc MTU:1500 Running Link Down
OHOS #
