本目录文件使用说明：
0.
    Master：//device/soc/hisilicon/hi3861v100/sdk_liteos/build/config/usr_config.mk
    LTS3.0：//device/hisilicon/hispark_pegasus/sdk_liteos/build/config/usr_config.mk
    将默认情况下“# CONFIG_I2C_SUPPORT is not set”，配置为“CONFIG_I2C_SUPPORT=y”，这样才会打开I2C功能的支持。

    Master：//device/soc/hisilicon/hi3861v100/sdk_liteos/app/wifiiot_app/init/app_io_init.c
    LTS3.0：//device/hisilicon/hispark_pegasus/sdk_liteos/app/wifiiot_app/init/app_io_init.c
    增加I2C0的初始化配置
    #ifdef CONFIG_I2C_SUPPORT
    /* I2C0 */
    hi_io_set_func(HI_IO_NAME_GPIO_13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
    hi_io_set_func(HI_IO_NAME_GPIO_14, HI_IO_FUNC_GPIO_14_I2C0_SCL);
    #endif

1. //applications/sample/wifi-iot/app/BUILD.gn
    "demolink:example_demolink",
    打开这个feature的编译，app/demolink/目录下的东西完全不需要改动，
    app/demolink/BUILD.gn 已经包含头文件路径了：
    "//domains/iot/link/libbuild",
    "//domains/iot/link/demolink"
    hellloworld.c也已经include头文件了，它直接调用了三方库libdemosdk.a提供的 DemoSdkEntry()。

2. //domains/iot/link/BUILD.gn
    "demolink:demolinkadapter"
    打开这个feature的编译，为三方库libdemosdk.a提供I2C_Init()/I2C_Write()接口的实现，
    这两个接口调用iot硬件子系统接口来实现功能，见
    //base/iot_hardware/peripheral/interfaces/kits/iot_i2c.h
    
    #"libbuild:demosdk",
    这个feature的编译要关闭，已经使用三方库libdemosdk.a了，这里不需要编译。

3. 本目录下的：
3.1 libdemosdk.a 拷贝到
    Master：//device/soc/hisilicon/hi3861v100/sdk_liteos/3rd_sdk/demolink/libs/
    LTS3.0：//device/hisilicon/hispark_pegasus/sdk_liteos/3rd_sdk/demolink/libs/
    目录下，这个三方库对上层暴露 DemoSdkEntry()接口，
    对底层要求实现I2C_Init()/I2C_Write()接口，其实现在 3.2
3.2 demolink/ 目录拷贝到 //domains/iot/link/ 目录下覆盖原有文件(或对比差异后同步过去)。

4. 编译hispark_pegasus项目，生成bin烧录到Hi3861开发板，reset开机后，
    平台显示屏显示：
    “Hello, OHOS！
     3rd-SDK Test
     libdemosdk.a”
    同时log每秒打印一次：“[demosdk] DemoSdkBiz: hello world.”
    这就表示三方库已经正常应用了。


     
     