#########################################################
[app_main] LiteOS-M Kernel(ROM) inited.
[app_main] app_main:: Begin:
[app_main] ::sdk ver:Hi3861V100R001C00SPC025 2020-09-03 18:10:00
peripheral_init.
hi_flash_init.
hi_uart_init.
[app_io_init] app_io_init. MACRO @usr_config.mk
        io:uart0
        io:uart1
        io:PWM
hi_pwm_init.
[app_main] ::hi_fs_init.
FileSystem mount ok.
[app_main] ::hi_at_init.
[hi_at] hi_at_init: in vendor, create task: at_proc & at_uart
[hi_at] hi_at_sys_cmd_register: cmd+exec
[app_main] ::tcpip init.
[app_main] ::wifi init success!
[app_main] ::HOS_SystemInit():
###########################################################
[system_init] HOS_SystemInit begin: %%%%%%%%%%%
[system_init] [7-1]: MODULE_INIT(bsp)======================
[system_init] [7-2]: MODULE_INIT(device)===================
[system_init] [7-3]: MODULE_INIT(core)=====================
[hiview_config] CORE_INIT_PRI(HiviewConfigInit, 0)        
[hiview_log] HiLogInit. CORE_INIT_PRI(HiLogInit, 0)
[wifi_device_util] InitWifiGlobalLock. CORE_INIT(InitWifiGlobalLock)

注册三个service
[system_init] [7-4]: SYS_INIT(service)=====================
[bootstrap_service] SYS_SERVICE_INIT(Init): Bootstrap
[samgr_lite] SAMGR_GetInstance(mutex=NULL): NO SAMGR instance, Init() to create ONE
[samgr_lite] Init. g_samgrImpl
[samgr_lite] Init. mutex[956100]. sharedPool[0-8] reset to 0. status[0-BOOT_SYS]
[samgr_lite] SAMGR_GetInstance(mutex=956100)
[samgr_lite] RegisterService(Name:Bootstrap)->Sid[0]

[broadcast_service] SYS_SERVICE_INIT(Init): Broadcast
[samgr_lite] RegisterService(Name:Broadcast)->Sid[1]

[hiview_service] SYS_SERVICE_INIT(Init): hiview
[samgr_lite] RegisterService(Name:hiview)->Sid[2]
[samgr_lite] RegisterFeatureApi(serviceName[hiview], feature[(null)])
[hiview_service] Init.InitHiviewComponent.

注册一个feature
[system_init] [7-5]: SYS_INIT(feature)=====================
[pub_sub_feature] Init. SYS_FEATURE_INIT(Init) g_broadcastFeature: Provider and subscriber
[samgr_lite] RegisterFeature(serviceName:Broadcast, featureName:Provider and subscriber)->Fid[0]
[pub_sub_implement] BCE_CreateInstance: set g_pubSubImplement.feature = &g_broadcastFeature
[samgr_lite] RegisterFeatureApi(serviceName[Broadcast], feature[Provider and subscriber])

[system_init] [7-6]: MODULE_INIT(run)======================
[helloworld] SYS_RUN(helloworld)
[led] SYS_RUN(LedEntry)

开始为记录在册的service创建运行条件：Queue和TaskPool
[system_init] [7-7]: SAMGR_Bootstrap()=====================
[samgr_lite] SAMGR_Bootstrap(status[1:BOOT_SYS_WAIT]). Begin:   size=3
        InitializeAllServices: unInited services: size=3
        ----------------------------------------------------
        Add service: Bootstrap   to TaskPool: 0x0...
                                    TaskPool: 0xfa488...
                                         Qid: 956424...
        InitializeSingleService(Bootstrap): SAMGR_SendSharedDirectRequest(handler[HandleInitRequest])
[message] SAMGR_SendSharedDirectRequest: Put Exchange into Qid:[956424],type[4], request.msgId[0]+msgValue[0]:
        ----------------------------------------------------
        Add service: Broadcast   to TaskPool: 0x0...
                                    TaskPool: 0xfaaf8...
                                         Qid: 956468...
        InitializeSingleService(Broadcast): SAMGR_SendSharedDirectRequest(handler[HandleInitRequest])
[message] SAMGR_SendSharedDirectRequest: Put Exchange into Qid:[956468],type[4], request.msgId[0]+msgValue[0]:
        ----------------------------------------------------
        Add service: hiview      to TaskPool: 0x0...
                                    TaskPool: 0xfacb8...
                                         Qid: 956512...
        InitializeSingleService(hiview): SAMGR_SendSharedDirectRequest(handler[HandleInitRequest])
[message] SAMGR_SendSharedDirectRequest: Put Exchange into Qid:[956512],type[4], request.msgId[0]+msgValue[0]:

开始为记录在册的service创建任务/线程
----------------------------------------------------
[task_manager] SAMGR_StartTaskPool:
        CreateTask[Bootstrap(Tid: 0xe87c0), size(2048), Prio(25)]-OK!
[task_manager] SAMGR_StartTaskPool:
        CreateTask[Broadcast(Tid: 0xe875c), size(2048), Prio(32)]-OK!
[task_manager] SAMGR_StartTaskPool:
        CreateTask[hiview(Tid: 0xe8824), size(2048), Prio(24)]-OK!
----------------------------------------------------
[samgr_lite] InitCompleted: services[3-0] inited, OK! END!

[samgr_lite] SAMGR_Bootstrap. End.
[system_init] HOS_SystemInit end. %%%%%%%%%%%%
###########################################################
[app_main] app_main End!
###########################################################



第一个service的线程启动并进入while(1)循环，监控消息队列，获取消息，
调用服务的Initialize对服务进行初始化，调用feature的OnInitialize对feature进行初始化
        TaskEntry(Qid:956468) into while(1) wait for MSG from queue....
        TaskEntry(Qid:956468) Recv MSG:
                request.msgId[0]+msgValue[0] -->> Sid[1],Fid[-1],Qid[0]
                type[4]:MSG_DIRECT/DirectRequest by handler
[samgr_lite] HandleInitRequest. to Init service:[Broadcast]Sid[1] and its features, updating Qid-->>
[broadcast_service] Initialize.[Sid:1, Fid:-1, Qid:956468]
[pub_sub_feature] OnInitialize(featureName[Provider and subscriber], [Sid:1, Fid:0, Qid:956468])
        -->>updated Qid[956468]
[samgr_lite] InitCompleted: services[3-0] inited, OK! END!

第二个service的线程启动并进入while(1)循环，监控消息队列，获取消息，
调用服务的Initialize对服务进行初始化，没有feature
        TaskEntry(Qid:956424) into while(1) wait for MSG from queue....
        TaskEntry(Qid:956424) Recv MSG:
                request.msgId[0]+msgValue[0] -->> Sid[0],Fid[-1],Qid[0]
                type[4]:MSG_DIRECT/DirectRequest by handler
[samgr_lite] HandleInitRequest. to Init service:[Bootstrap]Sid[0] and its features, updating Qid-->>
[bootstrap_service] Initialize.[Sid:0, Fid:-1, Qid:956424]
        -->>updated Qid[956424]
[samgr_lite] InitCompleted: services[3-2] inited, OK! END!

第三个service的线程启动并进入while(1)循环，监控消息队列，获取消息，
调用服务的Initialize对服务进行初始化，没有feature
        TaskEntry(Qid:956512) into while(1) wait for MSG from queue....
        TaskEntry(Qid:956512) Recv MSG:
                request.msgId[0]+msgValue[0] -->> Sid[2],Fid[-1],Qid[0]
                type[4]:MSG_DIRECT/DirectRequest by handler
[samgr_lite] HandleInitRequest. to Init service:[hiview]Sid[2] and its features, updating Qid-->>
[hiview_service] Initialize([Sid:2, Fid:-1, Qid:956512])
        -->>updated Qid[956512]
[samgr_lite] InitCompleted: services[3-3] inited, OK! ...

系统服务初始化完毕，status从 1->2，开始启动APP，由Bootstrap服务响应消息，去开启APP的启动
[samgr_lite] InitCompleted: status[1->2:BOOT_APP], all core system services Initialized!
        Going to SendBootRequest(msgId[0-BOOT_SYS_COMPLETED], msgValue:3)
[samgr_lite] SendBootRequest(to Bootstrap(Sid:0, Qid:956424), request.msgId[0]+msgValue[3]) ->Handler: SAMGR_Bootstrap()     
[message] SAMGR_SendRequest: Put Exchange into Qid:[956424],type[1], request.msgId[0]+msgValue[3]:


Bootstrap服务收到并通过MessageHandle来处理消息，去启动应用服务和应用feature
        TaskEntry(Qid:956424) Recv MSG:
                request.msgId[0]+msgValue[3] -->> Sid[0],Fid[-1],Qid[956512]
                type[1]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle
[bootstrap_service] MessageHandle(Bootstrap, request.msgId[0]+msgValue[3])
        case BOOT_SYS_COMPLETED[0]: flag[0]
        todo INIT_APP_CALL(service)/INIT_APP_CALL(feature)

注册service_example服务		
[service_example] SYSEX_SERVICE_INIT(Init). example_service
[samgr_lite] RegisterService(Name:example_service)->Sid[3]
[samgr_lite] RegisterFeatureApi(serviceName[example_service], feature[(null)])
注册service_example服务的example_feature
[feature_example] SYSEX_FEATURE_INIT(Init). example_service:example_feature
[samgr_lite] RegisterFeature(serviceName:example_service, featureName:example_feature)->Fid[0]
[samgr_lite] RegisterFeatureApi(serviceName[example_service], feature[example_feature])
                                -->>flag[1](0x01:LOAD_FLAG)
[message] SAMGR_SendResponseByIdentity(Sid[0],Fid[-1],Qid[956424]): request.msgId[0]+msgValue[3]
[message] SAMGR_SendResponse: Put Exchange into Qid:[956424],type[2], request.msgId[0]+msgValue[3]:

SAMGR_Bootstrap处理service_example和example_feature的启动
        TaskEntry(Qid:956424) Recv MSG:
                request.msgId[0]+msgValue[3] -->> Sid[0],Fid[-1],Qid[956424]
                type[2]:MSG_ACK/Response by SAMGR_Bootstrap
                response.data:"Bootstrap MessageHandle: BOOT_SYS_COMPLETED"
[samgr_lite] SAMGR_Bootstrap(status[3:BOOT_APP_WAIT]). Begin:   size=4
        InitializeAllServices: unInited services: size=1
        ----------------------------------------------------
创建example_service的Queue和TaskPool
        Add service: example_service     to TaskPool: 0x0...
                                    TaskPool: 0xf9e18...
                                         Qid: 956556...
        InitializeSingleService(example_service): SAMGR_SendSharedDirectRequest(handler[HandleInitRequest])
[message] SAMGR_SendSharedDirectRequest: Put Exchange into Qid:[956556],type[4], request.msgId[0]+msgValue[0]:
----------------------------------------------------
[task_manager] SAMGR_StartTaskPool:
启动example_service任务
        CreateTask[example_service(Tid: 0xe8504), size(2048), Prio(17)]-OK!
----------------------------------------------------
[samgr_lite] InitCompleted: services[4-3] inited, OK! END!
[samgr_lite] SAMGR_Bootstrap. End.

example_service任务进入while(1)监听消息队列
        TaskEntry(Qid:956556) into while(1) wait for MSG from queue....
        TaskEntry(Qid:956556) Recv MSG:
                request.msgId[0]+msgValue[0] -->> Sid[3],Fid[-1],Qid[0]
                type[4]:MSG_DIRECT/DirectRequest by handler
[samgr_lite] HandleInitRequest. to Init service:[example_service]Sid[3] and its features, updating Qid-->>
[service_example] Initialize
[feature_example] FEATURE_OnInitialize(service:example_service[Sid:3], feature:example_feature[Fid:0])
        -->>updated Qid[956556]
[samgr_lite] InitCompleted: services[4-4] inited, OK! ...

[samgr_lite] InitCompleted: status[3->4:BOOT_DYNAMIC], all system and application services Initialized!
        Going to SendBootRequest(msgId[1-BOOT_APP_COMPLETED], msgValue:4)
[samgr_lite] SendBootRequest(to Bootstrap(Sid:0, Qid:956424), request.msgId[1]+msgValue[4]) ->Handler: SAMGR_Bootstrap()
[message] SAMGR_SendRequest: Put Exchange into Qid:[956424],type[1], request.msgId[1]+msgValue[4]:


        TaskEntry(Qid:956424) Recv MSG:
                request.msgId[1]+msgValue[4] -->> Sid[0],Fid[-1],Qid[956556]
                type[1]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle
[bootstrap_service] MessageHandle(Bootstrap, request.msgId[1]+msgValue[4])
        case BOOT_APP_COMPLETED[1]: flag[1]
[message] SAMGR_SendResponseByIdentity(Sid[0],Fid[-1],Qid[956424]): request.msgId[1]+msgValue[4]
[message] SAMGR_SendResponse: Put Exchange into Qid:[956424],type[2], request.msgId[1]+msgValue[4]:
到这里，所有APP都已经启动完成。


        TaskEntry(Qid:956424) Recv MSG:
                request.msgId[1]+msgValue[4] -->> Sid[0],Fid[-1],Qid[956424]
                type[2]:MSG_ACK/Response by SAMGR_Bootstrap
                response.data:"Bootstrap MessageHandle: BOOT_APP_COMPLETED"
[samgr_lite] SAMGR_Bootstrap(status[5:BOOT_DYNAMIC_WAIT]). Begin:       size=4
[samgr_lite] InitCompleted: services[4-4] inited, OK! ...
[samgr_lite] InitCompleted: status[5->6:BOOT_DEBUG], Going to run TEST_CASE
        Going to SendBootRequest(msgId[3-BOOT_TEST_RUN], msgValue:4)
[samgr_lite] SendBootRequest(to Bootstrap(Sid:0, Qid:956424), request.msgId[3]+msgValue[4]) ->Handler: SAMGR_Bootstrap()     
[message] SAMGR_SendRequest: Put Exchange into Qid:[956424],type[1], request.msgId[3]+msgValue[4]:
[samgr_lite] SAMGR_Bootstrap. End.


        TaskEntry(Qid:956424) Recv MSG:
                request.msgId[3]+msgValue[4] -->> Sid[0],Fid[-1],Qid[956424]
                type[1]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle
[bootstrap_service] MessageHandle(Bootstrap, request.msgId[3]+msgValue[4])



这里开始跑 service_example.c 里的RunTestCase()
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        case BOOT_TEST_RUN[3]: flag[1]
[service_example] RunTestCase Begin:
[service_example] CASE_GetIUnknown: BEGIN
[service_example] CASE_GetIUnknown: GetDefaultFeatureApi(example_service) -> QueryInterface()
[service_example] CASE_GetIUnknown: END
[service_example] CASE_RegisterInvalidService: Service NG
[service_example] CASE_RegisterInvalidService: Feature NG
[service_example] CASE_RegisterInvalidService: DefaultFeatureApi NG
[service_example] CASE_RegisterInvalidService: FeatureApi NG
[service_example] CASE_SyncCall: BEGIN
[service_example] SyncCall Success!
[service_example] CASE_SyncCall: END
[service_example] CASE_ReleaseIUnknown: BEGIN
[service_example] CASE_ReleaseIUnknown: END
[service_example] RunTestCase End........

[feature_example] RunTestCase Begin:
[feature_example] CASE_GetIUnknown: example_service:example_feature BEGIN
[feature_example] CASE_GetIUnknown: Success
[feature_example] CASE_GetIUnknown: example_service:example_feature END
[feature_example] CASE_SyncCall: BEGIN
[feature_example] SyncCall(payload: Id[0], name[I wanna sync call good result!], value[1])
[feature_example] CASE_SyncCall: SyncCall Success
[feature_example] CASE_SyncCall: END
[feature_example] CASE_AsyncCall: BEGIN
[feature_example] AsyncCall: SAMGR_SendRequest(Sid[3]Fid[0]Qid[956556], request:msgId[0]msgValue[0])
[message] SAMGR_SendRequest: Put Exchange into Qid:[956556],type[0], request.msgId[0]+msgValue[0]:
[feature_example] CASE_AsyncCall: END
[feature_example] CASE_AsyncCallBack:
[feature_example] AsyncCallBack: SAMGR_SendRequest(Sid[3]Fid[0]Qid[956556], request:msgId[0]msgValue[0])
[message] SAMGR_SendRequest: Put Exchange into Qid:[956556],type[1], request.msgId[0]+msgValue[0]:
[feature_example] CASE_AsyncCallBack: Wait for response!
[feature_example] CASE_AsyncTimeCall: call AsyncTimeCall
[feature_example] AsyncTimeCall: SAMGR_SendRequest(Sid[3]Fid[0]Qid[956556], request:msgId[1]msgValue[1])
[message] SAMGR_SendRequest: Put Exchange into Qid:[956556],type[0], request.msgId[1]+msgValue[1]:
[feature_example] CASE_ReleaseIUnknown: BEGIN
[feature_example] CASE_ReleaseIUnknown: Success
[feature_example] CASE_ReleaseIUnknown: END
[feature_example] RunTestCase End........

[message] SAMGR_SendResponseByIdentity(Sid[0],Fid[-1],Qid[956424]): request.msgId[3]+msgValue[4]
[message] SAMGR_SendResponse: Put Exchange into Qid:[956424],type[2], request.msgId[3]+msgValue[4]:
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


        TaskEntry(Qid:956424) Recv MSG:
                request.msgId[3]+msgValue[4] -->> Sid[0],Fid[-1],Qid[956424]
                type[2]:MSG_ACK/Response by SAMGR_Bootstrap
                response.data:"Bootstrap MessageHandle: BOOT_TEST_RUN"
[samgr_lite] SAMGR_Bootstrap(status[7:BOOT_DEBUG_WAIT]). Begin:         size=4
[samgr_lite] InitCompleted: services[4-4] inited, OK! ...
[samgr_lite] SAMGR_Bootstrap. End.


        TaskEntry(Qid:956556) Recv MSG:
                request.msgId[0]+msgValue[0] -->> Sid[3],Fid[0],Qid[0]
                type[0]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle
[feature_example] FEATURE_OnMessage =================================================
[feature_example] FEATURE_OnMessage(feature:example_feature, request:msgId[0]msgValue[0]data[I wanna async call good result!])
[feature_example] FEATURE_OnMessage(msgId:0-MSG_PROC): SAMGR_SendResponse(request...)


        TaskEntry(Qid:956556) Recv MSG:
                request.msgId[0]+msgValue[0] -->> Sid[3],Fid[0],Qid[956424]
                type[1]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle				
[feature_example] FEATURE_OnMessage =================================================
[feature_example] FEATURE_OnMessage(feature:example_feature, request:msgId[0]msgValue[0]data[I wanna async call callback good result!])
[feature_example] FEATURE_OnMessage(msgId:0-MSG_PROC): SAMGR_SendResponse(request...)
[message] SAMGR_SendResponse: Put Exchange into Qid:[956424],type[2], request.msgId[0]+msgValue[0]:


        TaskEntry(Qid:956424) Recv MSG:
                request.msgId[0]+msgValue[0] -->> Sid[3],Fid[0],Qid[956424]
                type[2]:MSG_ACK/Response by SAMGR_Bootstrap
                response.data:"feature_example response"
[feature_example] AsyncHandler(request:msgId[0]msgValue[0], response:data[feature_example response])


        TaskEntry(Qid:956556) Recv MSG:
                request.msgId[1]+msgValue[1] -->> Sid[3],Fid[0],Qid[0]
                type[0]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle
[feature_example] FEATURE_OnMessage =================================================
[feature_example] FEATURE_OnMessage(feature:example_feature, request:msgId[1]msgValue[1]data[(null)])
[feature_example] FEATURE_OnMessage(msgId:1-MSG_TIME_PROC) LOS_Msleep(2) printCnt[1]
[samgr_maintenance] SAMGR_PrintServices Services(4) Information:
        [Sid:0]<status:1, name:Bootstrap, default:0x0, features:0, task:0xfa488>
                [TaskPool:0xfa488]<Tid:0xe87c0, Qid:0xe9808, ref:2, pri:25, stack:0x800>
        [Sid:1]<status:1, name:Broadcast, default:0x0, features:1, task:0xfaaf8>
                [TaskPool:0xfaaf8]<Tid:0xe875c, Qid:0xe9834, ref:1, pri:32, stack:0x800>
                [Fid:0]<name:Provider and subscriber, api:0xdc0d8>
        [Sid:2]<status:1, name:hiview, default:0xdc1dc, features:0, task:0xfacb8>
                [TaskPool:0xfacb8]<Tid:0xe8824, Qid:0xe9860, ref:1, pri:24, stack:0x800>
        [Sid:3]<status:2, name:example_service, default:0xdc160, features:1, task:0xf9e18>
                [TaskPool:0xf9e18]<Tid:0xe8504, Qid:0xe988c, ref:2, pri:17, stack:0x800>
                [Fid:0]<name:example_feature, api:0xdc128>
[feature_example] AsyncTimeCall: SAMGR_SendRequest(Sid[3]Fid[0]Qid[956556], request:msgId[1]msgValue[0])
[message] SAMGR_SendRequest: Put Exchange into Qid:[956556],type[0], request.msgId[1]+msgValue[0]:


        TaskEntry(Qid:956556) Recv MSG:
                request.msgId[1]+msgValue[0] -->> Sid[3],Fid[0],Qid[0]
                type[0]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle
[feature_example] FEATURE_OnMessage =================================================
[feature_example] FEATURE_OnMessage(feature:example_feature, request:msgId[1]msgValue[0]data[(null)])
[feature_example] FEATURE_OnMessage(msgId:1-MSG_TIME_PROC) LOS_Msleep(2) printCnt[2]
[samgr_maintenance] SAMGR_PrintOperations Services(4) Statistics:
        [Qid:0xe9808] <status:1, name:Bootstrap, abnormal:0, total:7, time:1280ms>
        [Qid:0xe9834] <status:1, name:Broadcast, abnormal:0, total:0, time:420ms>
        [Qid:0xe9860] <status:1, name:hiview, abnormal:0, total:0, time:500ms>
        [Qid:0xe988c] <status:2, name:example_service, abnormal:0, total:4, time:3410ms>
[feature_example] AsyncTimeCall: SAMGR_SendRequest(Sid[3]Fid[0]Qid[956556], request:msgId[1]msgValue[1])
[message] SAMGR_SendRequest: Put Exchange into Qid:[956556],type[0], request.msgId[1]+msgValue[1]:


        TaskEntry(Qid:956556) Recv MSG:
                request.msgId[1]+msgValue[1] -->> Sid[3],Fid[0],Qid[0]
                type[0]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle
[feature_example] FEATURE_OnMessage =================================================
[feature_example] FEATURE_OnMessage(feature:example_feature, request:msgId[1]msgValue[1]data[(null)])
[feature_example] FEATURE_OnMessage(msgId:1-MSG_TIME_PROC) LOS_Msleep(2) printCnt[3]
[samgr_maintenance] SAMGR_PrintServices Services(4) Information:
        [Sid:0]<status:1, name:Bootstrap, default:0x0, features:0, task:0xfa488>
                [TaskPool:0xfa488]<Tid:0xe87c0, Qid:0xe9808, ref:2, pri:25, stack:0x800>
        [Sid:1]<status:1, name:Broadcast, default:0x0, features:1, task:0xfaaf8>
                [TaskPool:0xfaaf8]<Tid:0xe875c, Qid:0xe9834, ref:1, pri:32, stack:0x800>
                [Fid:0]<name:Provider and subscriber, api:0xdc0d8>
        [Sid:2]<status:1, name:hiview, default:0xdc1dc, features:0, task:0xfacb8>
                [TaskPool:0xfacb8]<Tid:0xe8824, Qid:0xe9860, ref:1, pri:24, stack:0x800>
        [Sid:3]<status:2, name:example_service, default:0xdc160, features:1, task:0xf9e18>
                [TaskPool:0xf9e18]<Tid:0xe8504, Qid:0xe988c, ref:2, pri:17, stack:0x800>
                [Fid:0]<name:example_feature, api:0xdc128>
[feature_example] AsyncTimeCall: SAMGR_SendRequest(Sid[3]Fid[0]Qid[956556], request:msgId[1]msgValue[0])
[message] SAMGR_SendRequest: Put Exchange into Qid:[956556],type[0], request.msgId[1]+msgValue[0]:


        TaskEntry(Qid:956556) Recv MSG:
                request.msgId[1]+msgValue[0] -->> Sid[3],Fid[0],Qid[0]
                type[0]:0MSG_NON/1CON/3SYNC/Request by service MessageHandle

下面还有各个消息队列获取消息和处理消息的log，但都类似上面的。