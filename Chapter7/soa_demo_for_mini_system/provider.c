/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: liangkz @ https://ost.51cto.com/column/46
 * Date  : 2021.06.11
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>

//utils/native/lite/include
#include "ohos_init.h"

//kernel/liteos_m/kal/cmsis
#include "cmsis_os2.h"

#define SUPPORT_USRBTN_CTRL (1)
#if SUPPORT_USRBTN_CTRL
//base/iot_hardware/peripheral/interfaces/kits
#include "iot_gpio.h"

#define IOT_GPIO_5 (5)
#endif

//liangkz: BUILD.gn should include_dirs
//foundation/distributedschedule/samgr_lite/interfaces/kits/samgr,
//foundation/distributedschedule/samgr_lite/interfaces/kits/communication/broadcast,
#include "samgr_lite.h"
#include "broadcast_interface.h"

static uint32 g_button_press = 0;
static uint32 g_cycle_cnt    = 0;

static PubSubInterface *GetIUnknown(void)
{
    PubSubInterface *fapi = NULL;
    printf("[provider] GetIUnknown: BEGIN\n");

    IUnknown *iUnknown = SAMGR_GetInstance()->GetFeatureApi(BROADCAST_SERVICE, PUB_SUB_FEATURE);

    printf("\t GetFeatureApi()(*iUnknown) from service[%s]-feature[%s] %s\n",
           BROADCAST_SERVICE, PUB_SUB_FEATURE, (iUnknown == NULL)?"NG":"OK");
    if (iUnknown == NULL) {
        goto END;
    }

    int result = iUnknown->QueryInterface(iUnknown, DEFAULT_VERSION, (void **)&fapi);
    printf("\t QueryInterface()(PubSubInterface*) from (*iUnknown) %s\n",
        (result != 0 || fapi == NULL)?"NG":"OK: now we have Subscriber&&Provider APIs");

    if (result != 0 || fapi == NULL) {
        goto END;
    }

END:
    printf("[provider] GetIUnknown: END\n\n");

    return fapi;
}

static void PublishTopic(PubSubInterface *fapi)
{
    char buf[16];
    Topic topic = g_cycle_cnt%5;
    sprintf(buf, "cycleCnt:%d", g_cycle_cnt);

    printf("[provider] PublishTopic: topic[%d]/data[%s]\n",topic, buf);
    fapi->provider.Publish((IUnknown *)fapi, &topic, (uint8_t*)buf, 16);
}

static void *ProviderTask(const char *arg)
{
    (void)arg;    
    PubSubInterface *FeatureApi = GetIUnknown();

    while (1) 
    {
        g_cycle_cnt ++;

        if(g_button_press)
        {
            PublishTopic(FeatureApi);
            g_button_press = 0;
        }

        usleep(1000000);  //sleep 1s
    }

    return NULL;
}

static void UsrButtonPressed(char *arg)
{
    (void)arg;

    if(g_button_press == 0)
    {
        g_button_press = 1;
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        printf("[provider] UsrButtonPressed: detected! set g_button_press[T]\n");
    }

    return;
}

static void UsrButtonInit(void)
{
    IoTGpioInit(IOT_GPIO_5);
    IoTGpioSetFunc(IOT_GPIO_5, 0);   //HI_IO_FUNC_GPIO_5_GPIO
    IoTGpioSetDir(IOT_GPIO_5, IOT_GPIO_DIR_IN);
    IoTGpioSetPull(IOT_GPIO_5, IOT_IO_PULL_UP);
    IoTGpioRegisterIsrFunc(IOT_GPIO_5,               
                            IOT_INT_TYPE_EDGE,                
                            IOT_GPIO_EDGE_FALL_LEVEL_LOW,     
                            UsrButtonPressed,
                            NULL);
}

static void ProviderEntry(void)
{
    printf("[provider] SYS_RUN(ProviderEntry)\n");

    #if SUPPORT_USRBTN_CTRL
    UsrButtonInit();
    #endif

    osThreadAttr_t attr = {"ProviderTask", 0, NULL, 0, NULL, 4096, 25, 0, 0};

    if (osThreadNew((osThreadFunc_t)ProviderTask, NULL, &attr) == NULL) {
        printf("[provider] ProviderEntry: Falied to create ProviderTask!\n");
    }
}
SYS_RUN(ProviderEntry);