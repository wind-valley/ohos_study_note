/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: liangkz @ https://ost.51cto.com/column/46
 * Date  : 2021.06.11
 */

#include <ohos_init.h>
#include <securec.h>
#include <los_base.h>
#include <cmsis_os2.h>
#include <unistd.h>

#include "samgr_lite.h"
#include "broadcast_interface.h"

/*static*/ PubSubInterface *CASE_GetIUnknown(void);
/*static*/ void CASE_AddSubscribeTopic(PubSubInterface *fapi);


#define TEST_LEN 10
#define WAIT_PUB_PROC 1000
#define CONSUMER_SERVICE "consumer"

static void ConsumerCallback_0(Consumer *consumer, const Topic *topic, const Request *request)
{
    (void)consumer;
    (void)topic;

    printf("[consumer] ConsumerCallback 0(Sid[%d], Topic[%d], Request:msgId[%d]msgValue[%d]data[%s]) DO something BIG...\n",
        consumer->identity->serviceId, *topic, request->msgId, request->msgValue, (char *)request->data);
}

static void ConsumerCallback_1(Consumer *consumer, const Topic *topic, const Request *request)
{
    (void)consumer;
    (void)topic;

    printf("[consumer] ConsumerCallback 1(Sid[%d], Topic[%d], Request:msgId[%d]msgValue[%d]data[%s]) DO something GREAT...\n",
        consumer->identity->serviceId, *topic, request->msgId, request->msgValue, (char *)request->data);
}

static void ConsumerCallback_2(Consumer *consumer, const Topic *topic, const Request *request)
{
    (void)consumer;
    (void)topic;

    printf("[consumer] ConsumerCallback 2(Sid[%d], Topic[%d], Request:msgId[%d]msgValue[%d]data[%s]) DO something BEYOND IMAGINATION...\n",
        consumer->identity->serviceId, *topic, request->msgId, request->msgValue, (char *)request->data);
}

static BOOL Equal(const Consumer *current, const Consumer *other)
{
    return  (current->Notify == other->Notify);
}

static const char *GetName(Service *service)
{
    (void)service;
    return CONSUMER_SERVICE;
};

static Identity g_identity = { -1, -1, NULL};  //Sid,Fid,Qid
static BOOL Initialize(Service *service, Identity identity)
{
    printf("[consumer] Initialize %s: g_identity[Sid:%d,Fid:%d,Qid:%d]\n",
        CONSUMER_SERVICE,
        identity.serviceId, identity.featureId, identity.queueId);
    
    g_identity = identity;
    (void)service;

    return TRUE;
};

static BOOL MessageHandle(Service *service, Request *msg)
{
    printf("[consumer] MessageHandle(serviceName[%s], Request:msgId[%d]msgValue[%d]data[%s])\n", 
        service->GetName(service), msg->msgId, msg->msgValue, (char *)msg->data);

    (void)service;
    return FALSE;
};

static TaskConfig GetTaskConfig(Service *service)
{
    TaskConfig config = {LEVEL_HIGH, PRI_ABOVE_NORMAL, 0x800, 20, SHARED_TASK};
    (void)service;
    return config;
};

static Service g_ConsumerService = {GetName, Initialize, MessageHandle, GetTaskConfig};
static void Init(void)
{
    printf("[consumer]        SYSEX_SERVICE_INIT(Init)# %s\n",CONSUMER_SERVICE);

    SAMGR_GetInstance()->RegisterService(&g_ConsumerService);

    printf("[consumer] ======================================\n");
    //get a PubSubInterface obj pointer
    PubSubInterface *FeatureApi = CASE_GetIUnknown();
    
    //use the obj pointer to add subscribe to topic
    CASE_AddSubscribeTopic(FeatureApi);
    printf("[consumer] ======================================\n");
}
SYSEX_SERVICE_INIT(Init);

/*static*/ PubSubInterface *CASE_GetIUnknown(void)
{
    PubSubInterface *fapi = NULL;
    printf("[consumer] CASE_GetIUnknown: BEGIN\n");

    IUnknown *iUnknown = SAMGR_GetInstance()->GetFeatureApi(BROADCAST_SERVICE, PUB_SUB_FEATURE);
    printf("\t GetFeatureApi()(*iUnknown) from service[%s]-feature[%s] %s\n",
           BROADCAST_SERVICE, PUB_SUB_FEATURE, (iUnknown == NULL)?"NG":"OK");
    if (iUnknown == NULL) {
        goto END;
    }

    int result = iUnknown->QueryInterface(iUnknown, DEFAULT_VERSION, (void **)&fapi);
    printf("\t QueryInterface()(PubSubInterface*) from (*iUnknown) %s\n",
        (result != 0 || fapi == NULL)?"NG":"OK: now we have Subscriber&&Provider APIs");

    if (result != 0 || fapi == NULL) {
        goto END;
    }

END:
    printf("[consumer] CASE_GetIUnknown: END\n");

    return fapi;
}

/*static*/ void CASE_AddSubscribeTopic(PubSubInterface *fapi)
{
    Subscriber *subscriber = &fapi->subscriber;

    static Consumer c0 = {.identity = &g_identity, .Notify = ConsumerCallback_0, .Equal = Equal};
    static Consumer c1 = {.identity = &g_identity, .Notify = ConsumerCallback_1, .Equal = Equal};
    static Consumer c2 = {.identity = &g_identity, .Notify = ConsumerCallback_2, .Equal = Equal};

    //A topic can be subscribed to only after being added.
    printf("[consumer] CASE_AddSubscribeTopic: BEGIN\n");

    Topic topic0 = 0;
    printf("[consumer] AddTopic[0] Subscribe: no Consumer------------\n");
    printf("\tsubscriber: [1-1] AddTopic : topic[0]\n");
    subscriber->AddTopic( (IUnknown *)fapi, &topic0);


    Topic topic1 = 1;
    printf("[consumer] AddTopic[1] Subscribe: Consumer[0]------------\n");
    printf("\tsubscriber: [2-1] AddTopic : topic[1]\n");
    subscriber->AddTopic( (IUnknown *)fapi, &topic1);

    printf("\tsubscriber: [2-2] Subscribe: topic[1]:Consumer[0]\n");
    subscriber->Subscribe((IUnknown *)fapi, &topic1, &c0);


    Topic topic2 = 2;
    printf("[consumer] AddTopic[2] Subscribe:Consumer[0][1]---------\n");
    printf("\tsubscriber: [3-1] AddTopic : topic[2]\n");
    subscriber->AddTopic((IUnknown *)fapi,  &topic2);

    printf("\tsubscriber: [3-2] Subscribe: topic[2]:Consumer[0]\n");
    subscriber->Subscribe((IUnknown *)fapi, &topic2, &c0);

    printf("\tsubscriber: [3-3] Subscribe: topic[2]:Consumer[1]\n");
    subscriber->Subscribe((IUnknown *)fapi, &topic2, &c1);


    Topic topic3 = 3;
    printf("[consumer] AddTopic[3] Subscribe:Consumer[0][1][2]------\n");
    printf("\tsubscriber: [4-1] AddTopic : topic[3]\n");
    subscriber->AddTopic((IUnknown *)fapi,  &topic3);

    printf("\tsubscriber: [4-2] Subscribe: topic[3]:Consumer[0]\n");
    subscriber->Subscribe((IUnknown *)fapi, &topic3, &c0);

    printf("\tsubscriber: [4-3] Subscribe: topic[3]:Consumer[1]\n");
    subscriber->Subscribe((IUnknown *)fapi, &topic3, &c1);

    printf("\tsubscriber: [4-4] Subscribe: topic[3]:Consumer[2]\n");
    subscriber->Subscribe((IUnknown *)fapi, &topic3, &c2);

    printf("[consumer] CASE_AddSubscribeTopic: END\n");
}